1. uintptr_t , jer kastanjem fizicke adrese u pointer, on nju interpretira kao da je to virtuelna adresa
   samim time u datom virtuelnom prostoru moguce je da pomocu pointera dodjemo do sasvim druge fizicke lokacije
   u memorije od one date sa fizickom adresom

2. pde-ovi od 1023 - 256 do 1023 su mapirani u fizicke okvire u opsegu od 0 do 256 * 1024 - 1 od KB // KERNEL
   pde-ovi od // KERNSTACK
   pde-ovi od // data PD
   pde-ovi od // pages

3. mapiranja koja koristi kernel su stavljena bez user permisije !U, sto znaci kad se pomoguce segmenata CPUovi
   prebaci u 3 novi privilegija - USER, svaki pokusaj pristupanja VA od kernela dovest ce PGE permission general fault

4. 256MB, da bi vrsili translaciju fizickih u virtuelne adrese i obratno moramo imati dati opseg RAMa mapiram negdje u VA
   npr. kada kernel alocira stranicu na fizickoj adresi od 300mb za neki proces, ta bi na toj stranici izvrsio podesavanja
   pojedinih njenih djelova u skladu sa nekim strukturama, mora imati mogucnost da u svom VA prostoru pristupi toj PA, sto znaci
   da ako kernel mapira 0 do 256mb u adrese od 4gb -256 mb do 4gb, a ostali ram ne mapira, tom ramu ne moze ni pristupiti, u konfiguraciji
   u kojoj je dat JOS, zbog toga je intel kasnije uveo PAE , ekstenziju sabirnice sa jos 4 linije, pa samim time mozemo adresirati 64gb
   sto znaci da su nam sva 4gb dostupna ako npr mapiramo ih od 4 do 8gb

5. For memory managment JOS need 2 pointers, which size is 4 bytes each. Further more, for every 4KB JOS allocates sizeof(PageInfo) memory to
   hold info about that page. So, in total we need size_of_RAM/4KB * sizeof(PageInfo) memory to hold informations about pages.
   In my new allocation system, which allows granularity of pages from 4KB to 4MB with step on 2^N, to hold informations about allocated pages
   JOS need size_of_RAM / 4MB * 2^(4MB/4KB + 1) = size_of_RAM / 4MB * 2KB
  // mrsko mi je prevoditi ostale odgovore na engleski hh

6. _start je mapiran na VA 1MB + 12bajti, kada ukljucimo paging, imamo mapiranje od 0 do 4mb i kernbase + kernbase + 4mb u 0 do 4mb fizicke adrese
   , kernel je linkan na kernabase adresu, sto znaci da su svi njegovi labeli podeseni na adrese kernbase, osim starta_, sada kad ukljucimo pageing
   moguce je pozvati bilo koju funkciju od kernela, jer je imamo mapiranje tih adresa na stvarnu lokaciju u memoriji gdje se nalaze dati djelovi koda
   od kernela, tranzicija je neophodna kako bi mogli izvrsavati kernel kod, mapiranje 0 do 4mb je potrebno ostaviti jer je se PG i PTovi njeni nalaze
   negdje u  opsgegu od 0 do 4MB, samim tim moramo imati to mapiranje da bi mogli pristupiti tabeli mapiranja, jer je adresa pohranjena u cr3 u
   sustini fizicka adresa PGa


