/*	$NetBSD: stdarg.h,v 1.12 1995/12/25 23:15:31 mycroft Exp $	*/

#ifndef JOS_INC_STDARG_H
#define	JOS_INC_STDARG_H

typedef __builtin_va_list va_list; // tip koji se koristi kao ulaz za sve dole navedene makroe koji su definisani u gcc kompajleru

#define va_start(ap, last) __builtin_va_start(ap, last)
//postavlja ap da pokazuje na prvi opcionalni argument , a last mora biti posljednji 'zahtjevani' argument -- argument koji se sigurno mora pojaviti pri pozivu funkcije
#define va_arg(ap, type) __builtin_va_arg(ap, type)
// vraca argument type ako se odradilo uspjesno citanje, a u pozadini je ap postavljen na novi argument
#define va_end(ap) __builtin_va_end(ap)
//indikator za kraj koristenja ap, treba se pozvati prije kraja funkcije u kojem je isti ap incijalizovan
#endif	/* !JOS_INC_STDARG_H */
