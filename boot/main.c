#include <inc/x86.h>
#include <inc/elf.h>

/**********************************************************************
 * This a dirt simple boot loader, whose sole job is to boot
 * an ELF kernel image from the first IDE hard disk.
 *
 * DISK LAYOUT
 *  * This program(boot.S and main.c) is the bootloader.  It should
 *    be stored in the first sector of the disk.
 *
 *  * The 2nd sector onward holds the kernel image.
 *
 *  * The kernel image must be in ELF format.
 *
 * BOOT UP STEPS
 *  * when the CPU boots it loads the BIOS into memory and executes it
 *
 *  * the BIOS intializes devices, sets of the interrupt routines, and
 *    reads the first sector of the boot device(e.g., hard-drive)
 *    into memory and jumps to it.
 *
 *  * Assuming this boot loader is stored in the first sector of the
 *    hard-drive, this code takes over...
 *
 *  * control starts in boot.S -- which sets up protected mode,
 *    and a stack so C code then run, then calls bootmain()
 *
 *  * bootmain() in this file takes over, reads in the kernel and jumps to it.
 **********************************************************************/

#define SECTSIZE	512 /*Velicina sektora na disku u trenutnom formatu je 512 bajti*/
#define ELFHDR		((struct Elf *) 0x10000) // scratch space
/* Kernel ce biti ucitan na adresu 0x10000 */
void readsect(void*, uint32_t);
void readseg(uint32_t, uint32_t, uint32_t);

void
bootmain(void)
{
	struct Proghdr *ph, *eph;

	// read 1st page off disk
	readseg((uint32_t) ELFHDR, SECTSIZE*8, 0);

	// is this a valid ELF?
	if (ELFHDR->e_magic != ELF_MAGIC) /*Svaki elf file pocinje sa 32bitnom vrijednoscu koja predstavlja konstantu koja vrsi validaciju da li je dati file elf file*/
		goto bad;

  // program header se sastoji od niza Proghdr struktura pri cemu svaki opisuje pojedini dio koda
	// load each program segment (ignores ph flags)
	ph = (struct Proghdr *) ((uint8_t *) ELFHDR + ELFHDR->e_phoff); // kastuje adresu na kojoj je ucitan kernel u char pointer 
                                                                  //te joj dodaje broj bajta u elf file od kojeg pocinje prog header
	eph = ph + ELFHDR->e_phnum; // e_phnum daje broj stavki u program header tablici
	for (; ph < eph; ph++)
		// p_pa is the load address of this segment (as well
		// as the physical address)
		readseg(ph->p_pa, ph->p_memsz, ph->p_offset);
   // p_pa - physical addres
   // p_memszy - velicina segmenta u memoriji
   // p_offset - offset od pocetka file na kojeg prvi bit se nalazi

	// call the entry point from the ELF header
    // note: does not return!
	((void (*)(void)) (ELFHDR->e_entry))(); // drzi virtuelnu adresu odakle pocinje izvrsavanje programa

bad:
	outw(0x8A00, 0x8A00); // 0x8A00 port za debbugiranje, 0x8A00 omogucuje port za debbugiranje
	outw(0x8A00, 0x8E00); // neka poruka na port za debugiranje
	while (1)
		/* do nothing */;
}

// Read 'count' bytes at 'offset' from kernel into physical address 'pa'.
// Might copy more than asked
// readseg( adresa pocetka , broj_bita ,   )
void
readseg(uint32_t pa, uint32_t count, uint32_t offset) /*Argumenti za prvi poziv u ovom fajlu (0x10000,512*8,0)*/ 
{
	uint32_t end_pa;

	end_pa = pa + count;

	// round down to sector boundary
	pa &= ~(SECTSIZE - 1);
  /*SECTSIZE je 512, pa je ovo ekvivalentno sa ~(0x1ff) & pa , sto je jednako cjelobrojnom djeljenju sa 511(bez ostatka) - zaokruzuje na cjeli broj sektora prema dole*/

	// translate from bytes to sectors, and kernel starts at sector 1
	offset = (offset / SECTSIZE) + 1;

	// If this is too slow, we could read lots of sectors at a time.
	// We'd write more to memory than asked, but it doesn't matter --
	// we load in increasing order.
	while (pa < end_pa) {
		// Since we haven't enabled paging yet and we're using
		// an identity segment mapping (see boot.S), we can
		// use physical addresses directly.  This won't be the
		// case once JOS enables the MMU.
		readsect((uint8_t*) pa, offset);
		pa += SECTSIZE; /*Pomjeramo se u memoriji za velicinu jednog sektora*/ 
		offset++; /*mjenjamo broj sektora kojeg citamo za po 1*/
	}
}

void
waitdisk(void)
{
	// wait for disk reaady
	while ((inb(0x1F7) & 0xC0) != 0x40) // byte & 11000000 != 01000000 dakle petlja se vrti dok najaca dva bajta nisu 01
                                      // 6 bit (indexom) je na 0 ako se desila greska ili ako se brzina vrtnje magnetnog diska smanjila
                                      // 7 bit (indexom) je na 1 ako je disk zauzet nekom drugom operacijom i treba cekati da vrijednost ne postane 0
		/* do nothing */;
}

void
readsect(void *dst, uint32_t offset)
{
	// wait for disk to be ready
	waitdisk();
  /*outb(int port,char data)*/ 
    outb(0x1F2, 1);		// count = 1  sector count
    outb(0x1F3, offset); //  sector number
    outb(0x1F4, offset >> 8); // cylinder low
    outb(0x1F5, offset >> 16); // cylinder high
    outb(0x1F6, (offset >> 24) | 0xE0); // drive head port, moze podrzavat extra adresiranje
    outb(0x1F7, 0x20);	// cmd 0x20 - read sectors // command port 
   
    // ATA portovi su 0x1f0 do 0x1f7 i 0x3f6

    // wait for disk to be ready
    waitdisk();

    // read a sector
    insl(0x1F0, dst, SECTSIZE/4); // citati sa DATA porta u destinaciju , 512/4 puta po 4 bajta
   }

