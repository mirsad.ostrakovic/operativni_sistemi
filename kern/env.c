/* See COPYRIGHT for copyright information. */

#include <inc/x86.h>
#include <inc/mmu.h>
#include <inc/error.h>
#include <inc/string.h>
#include <inc/assert.h>
#include <inc/elf.h>

#include <kern/env.h>
#include <kern/pmap.h>
#include <kern/trap.h>
#include <kern/monitor.h>
#include <kern/sched.h>
#include <kern/cpu.h>
#include <kern/spinlock.h>

struct Env *envs = NULL;		// All environments
static struct Env *env_free_list;	// Free environment list
					// (linked by Env->env_link)

#define ENVGENSHIFT	12		// >= LOGNENV

// Global descriptor table.
//
// Set up global descriptor table (GDT) with separate segments for
// kernel mode and user mode.  Segments serve many purposes on the x86.
// We don't use any of their memory-mapping capabilities, but we need
// them to switch privilege levels. 
//
// The kernel and user segments are identical except for the DPL.
// To load the SS register, the CPL must equal the DPL.  Thus,
// we must duplicate the segments for the user and the kernel.
//
// In particular, the last argument to the SEG macro used in the
// definition of gdt specifies the Descriptor Privilege Level (DPL)
// of that descriptor: 0 for kernel and 3 for user.
//

struct Segdesc gdt[NCPU + 5] =

{
	// 0x0 - unused (always faults -- for trapping NULL far pointers)
	SEG_NULL,

	// 0x8 - kernel code segment
	[GD_KT >> 3] = SEG(STA_X | STA_R, 0x0, 0xffffffff, 0),

	// 0x10 - kernel data segment
	[GD_KD >> 3] = SEG(STA_W, 0x0, 0xffffffff, 0),

	// 0x18 - user code segment
	[GD_UT >> 3] = SEG(STA_X | STA_R, 0x0, 0xffffffff, 3),

	// 0x20 - user data segment
	[GD_UD >> 3] = SEG(STA_W, 0x0, 0xffffffff, 3),

	// Per-CPU TSS descriptors (starting from GD_TSS0) are initialized
	// in trap_init_percpu()
	[GD_TSS0 >> 3] = SEG_NULL
};

// Da bi global descriptor tabela koristila potrebno da je sa memorijske lokacije date instrukciji lgdt
// procitamo 6 bajta i smjestimo ih u GDT segmentni deskriptor ( registar od 6 bajta ), koji formatiran
// tako da prva dva bajta = 16 bita sadrze velicinu tabele , a ostala 4 bajta = 32 bita sadrze linearnu adresu gdt

struct Pseudodesc gdt_pd = {
	sizeof(gdt) - 1, (unsigned long) gdt
};

//
// Converts an envid to an env pointer.
// If checkperm is set, the specified environment must be either the
// current environment or an immediate child of the current environment.
//
// RETURNS
//   0 on success, -E_BAD_ENV on error.
//   On success, sets *env_store to the environment.
//   On error, sets *env_store to NULL.
//

// envid je jedinstveni kljuc koje dobija svaki proces pri pokretanju
// prvih 10 bita tog kljuca cini broj koji predstavlja index datog env framea 
// broj okuzenja limitiran na NENV = 1024, samim tim koristi se 10 bajti

// ako je frame okruzenja slobodno , samim tim  njegova polja su nevalidna i vraca se -E_BAD_NENV
// dakle okruzenje sa datim idom je postojalo ali je zatvoreno iz nekog razloga
// ili ako env frame nije slobodan tj. koristi se ali se idovi razlikuju, znaci da je envid - agument funkcije
// id od starog procesa koji je koristio isti env frame , takodjer se vraca -E_BAD_NENV

// data funkcija moze vrsiti jos jednu provjeru, i to ako je checkperm = 1
// funkcija provjerava da li okruzenje sa navedenim id-om pored gore nevednih specifikacija da postoji 
// takodjer mora biti proces koji se  trenutno izvrsava, ili dijete tog procesa ( curenv je na neki nacin kreiralo taj proces )

int
envid2env(envid_t envid, struct Env **env_store, bool checkperm)
{
	struct Env *e;

	// If envid is zero, return the current environment.
	if (envid == 0) {
		*env_store = curenv;
		return 0;
	}

	// Look up the Env structure via the index part of the envid,
	// then check the env_id field in that struct Env
	// to ensure that the envid is not stale
	// (i.e., does not refer to a _previous_ environment
	// that used the same slot in the envs[] array).
	e = &envs[ENVX(envid)];
	if (e->env_status == ENV_FREE || e->env_id != envid) {
		*env_store = 0;
		return -E_BAD_ENV;
	}


	// Check that the calling environment has legitimate permission
	// to manipulate the specified environment.
	// If checkperm is set, the specified environment
	// must be either the current environment
	// or an immediate child of the current environment.
	if (checkperm && e != curenv && e->env_parent_id != curenv->env_id) {
		*env_store = 0;
		return -E_BAD_ENV;
	}

	*env_store = e;
	return 0;
}

// Mark all environments in 'envs' as free, set their env_ids to 0,
// and insert them into the env_free_list.
// Make sure the environments are in the free list in the same order
// they are in the envs array (i.e., so that the first call to
// env_alloc() returns envs[0]).
//

// Data funkcija treba da ulinka okruzenja u listu tako da na pocetku elementi u nizu envs
// i listi env_free_list budu u istom poretku

void
env_init(void)
{
	// Set up envs array
	// LAB 3: Your code here.
     
  envs[NENV-1].env_link = NULL; // zadnje orkuzenje ne pokazuje nigdje
  for( uint32_t i = NENV - 1 ; i > 0 ; --i ){ // svako prethodno okruzenje pokazuje na sljedece
    envs[i-1].env_link = &envs[i];
    envs[i].env_id = 0;
  }
  envs[0].env_id = 0;
  env_free_list = &envs[0]; // env_free_list pokazuje na prvo okruzenje

	// Per-CPU part of the initialization
	env_init_percpu(); // funkcija koja obavlja inicijalizaciju ostalih stvari koje su potrebne za pokretanje procesa, trenutnog cpu jezgra
}



// Load GDT and segment descriptors.
void
env_init_percpu(void)
{
	lgdt(&gdt_pd); // ldgt koristi inline asembly, tj instrukcij lgdt koja uzima adresu na kojoj se nalazi deskriptor za GDT deskriptor registar
	
  // The kernel never uses GS or FS, so we leave those set to
	// the user data segment.
  
  // dole navedene instrukcije su inline asembly, u njemu se registri navode sa %% , a varijable sa % , poslije prve : idu izlazni parametri
  // nakon drugo : ulazni parametri i poslije : (ako postoji) idu naznake kao npr da ce instrukcija mijenjati memoriju itd.
	asm volatile("movw %%ax,%%gs" : : "a" (GD_UD|3)); // postavlja se vrijednost gs i fs na vrijednost user data segemnata
	asm volatile("movw %%ax,%%fs" : : "a" (GD_UD|3)); // GD_UD je index destkiptor u tabeli gdt shiftan za 3, OR 3 operacije ide da bi se RPL postavio na USER
	// The kernel does use ES, DS, and SS.  We'll change between
	// the kernel and user data segments as needed.
  //
	asm volatile("movw %%ax,%%es" : : "a" (GD_KD)); // ostali segemntni registri se postavljaju na vrijednost kernel data desktiptora
	asm volatile("movw %%ax,%%ds" : : "a" (GD_KD));
	asm volatile("movw %%ax,%%ss" : : "a" (GD_KD));
	// Load the kernel text segment into CS.
	asm volatile("ljmp %0,$1f\n 1:\n" : : "i" (GD_KT)); // posebno CS se postavlja na vrijednost kernel text (code) deskriptora
                                                      // i to pomocu instrukcije long jump, koja je jedna od nacina da se promjeni CS
	// For good measure, clear the local descriptor table (LDT),
	// since we don't use it.
	
  lldt(0); // postavlja segment koji pokacuje na ldt na 0
}

//
// Initialize the kernel virtual memory layout for environment e.
// Allocate a page directory, set e->env_pgdir accordingly,
// and initialize the kernel portion of the new environment's address space.
// Do NOT (yet) map anything into the user portion
// of the environment's virtual address space.
//
// Returns 0 on success, < 0 on error.  Errors include:
//	-E_NO_MEM if page directory or table could not be allocated.
//


// JOS koristi za svaki proces isti memory layout, pri cemu je mapiranje kernel dijela identicno mapiranju kern_pgdir
// dok se mijenja mapiranje user dijela, u zavisnosti od layout svakog procesa
// funckija env_setup_vm postavlja mapiranje za dati procesi isto onom u VA prostoru od kernela, zasto?
// da bi se iznimke mogle brze tretirati, jer kada ne bi imali dato mapiranje morali bi svaki put pri tretmanu iznimke
// prebaciti se iz VA prosora od procesa od kernela, odraditi rutinu i vratiri se, sto se desava vrlo cesto, dakle 
// dato rjesenje nije "optimalno", iako je moguce za kreirati, challeng zadatak 4 sa laba 2, postavlja ovo kao problem


static int
env_setup_vm(struct Env *e)
{
	int i;
	struct PageInfo *p = NULL;

	// Allocate a page for the page directory
	if (!(p = page_alloc(ALLOC_ZERO))) // alocita PD za tadi proces 
		return -E_NO_MEM; // ako alokacija nije uspjesna vratiti -E_N0_MEM

	// Now, set e->env_pgdir and initialize the page directory.
	//
	// Hint:
	//    - The VA space of all envs is identical above UTOP
	//	(except at UVPT, which we've set below).
	//	See inc/memlayout.h for permissions and layout.
	//	Can you use kern_pgdir as a template?  Hint: Yes.
	//	(Make sure you got the permissions right in Lab 2.)
	//    - The initial VA below UTOP is empty.
	//    - You do not need to make any more calls to page_alloc.
	//    - Note: In general, pp_ref is not maintained for
	//	physical pages mapped only above UTOP, but env_pgdir
	//	is an exception -- you need to increment env_pgdir's
	//	pp_ref for env_free to work correctly.
	//    - The functions in kern/pmap.h are handy.

	// LAB 3: Your code here.
  ++p->pp_ref; // uvecamo ref_count od date stranice
  e->env_pgdir = (pde_t *)page2kva(p); // datu stranicu dodjelimo pointeru na pgdir od procesa
  
  // JOS ne dealocira stranice koje se koriste za kernel mapiranje pri oslobadjanju resursa od procesa
  // pa stoga mozemo PTE od kern_pgdir prekopira u pgdir od procesa sve do UTOP-a ( nema smisla ici ispod toga je te stranice u kernel VA nisu mapirane )
  for( uint32_t i = NPDENTRIES - 1 ; i >= UTOP / PTSIZE ; --i ) // >= jer je UTOP = UENVS, pa da bi i UENVS mapirali
    e->env_pgdir[i] = kern_pgdir[i];
     
	// UVPT maps the env's own page table read-only.
	// Permissions: kernel R, user R
  
	e->env_pgdir[PDX(UVPT)] = PADDR(e->env_pgdir) | PTE_P | PTE_U; // jedina razlika u odnosu na kernel, je sto user mapira kao pde na indexu PDX(UVPT) svoju VA, dakle
  // kada zelimo doci do fizicke adrese PD od procesa mozemo to ucitniti ako procitamo PA od PTE na PDX(UVPT) , sto znaci PT od datog PDE na indexu PDX(UVPT) je
  // ponovo taj isti PD 

	return 0;
}

//
// Allocates and initializes a new environment.
// On success, the new environment is stored in *newenv_store.
//
// Returns 0 on success, < 0 on failure.  Errors include:
//	-E_NO_FREE_ENV if all NENVS environments are allocated
//	-E_NO_MEM on memory exhaustion


// funkcija kola alocira okruzenje
int
env_alloc(struct Env **newenv_store, envid_t parent_id)
{
	int32_t generation;
	int r;
	struct Env *e;

	if (!(e = env_free_list)) // ako nema slobodnog framea od okruzenja, novi proces se ne moze kreirati, inace se uzima prvo okruzenje sa liste
		return -E_NO_FREE_ENV;

	// Allocate and set up the page directory for this environment.
	if ((r = env_setup_vm(e)) < 0) // ako postavljanje VA prostora od procesa iznad UTOPa ( tj mapiranje kernela u user VA ), vratiti gresku
		return r;

  //
  // env_id je tipa int, da bi id bio validan on  mora biti pozitivan, tj najjaci bit mora bit u id mora biti 0 
  //
  //
  
	// Generate an env_id for this environment.
	generation = (e->env_id + (1 << ENVGENSHIFT)) & ~(NENV - 1); //  = ROUNDDOWN( ( e->env_id + 4096 ) , 1024 )
	if (generation <= 0)	// Don't create a negative env_id. 
		generation = 1 << ENVGENSHIFT; // ako je generation < 0 , znaci da se desio overflow, dakle proces koji je koristio imao je uniquifier blizu max vrijednosti
	e->env_id = generation | (e - envs); // dakle kao novi uniquifer mozemo koristiti broj koji blizu starta, tj konrekno broj 4
                                      // e - envs je index okruzenja 

	// Set the basic status variables.
	e->env_parent_id = parent_id; // postavlja se id rodtilje, tj procesa koji je kreirao dati proces
	e->env_type = ENV_TYPE_USER;  // postavlja se tip procesa na user
	e->env_status = ENV_RUNNABLE; // status se postavlja na runnable
	e->env_runs = 0; // broj pokretanja okruzenja se stavlja na 0

	// Clear out all the saved register state,
	// to prevent the register values
	// of a prior environment inhabiting this Env structure
	// from "leaking" into our new environment.
	memset(&e->env_tf, 0, sizeof(e->env_tf)); // prebrise se stari trapframe sa 0

	// Set up appropriate initial values for the segment registers.
	// GD_UD is the user data segment selector in the GDT, and
	// GD_UT is the user text segment selector (see inc/memlayout.h).
	// The low 2 bits of each segment register contains the
	// Requestor Privilege Level (RPL); 3 means user mode.  When
	// we switch privilege levels, the hardware does various
	// checks involving the RPL and the Descriptor Privilege Level
	// (DPL) stored in the descriptors themselves.
	e->env_tf.tf_ds = GD_UD | 3; // postavljaju se segmentni registri na user segmentne deskriptore
	e->env_tf.tf_es = GD_UD | 3;
	e->env_tf.tf_ss = GD_UD | 3;
	e->env_tf.tf_esp = USTACKTOP; // esp se postavlja na vrh user stacka koji je inace velicine 4kb
	e->env_tf.tf_cs = GD_UT | 3;
	// You will set e->env_tf.tf_eip later.

	// Enable interrupts while in user mode.
	// LAB 4: Your code here.
  e->env_tf.tf_eflags =  FL_IF; 

	// Clear the page fault handler until user installs one.
	e->env_pgfault_upcall = 0;

	// Also clear the IPC receiving flag.
	e->env_ipc_recving = 0;

	// commit the allocation
	env_free_list = e->env_link; // env_free_list na sljedece slobodno okruzenje
	*newenv_store = e;

	// cprintf("[%08x] new env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
	return 0;
}

//
// Allocate len bytes of physical memory for environment env,
// and map it at virtual address va in the environment's address space.
// Does not zero or otherwise initialize the mapped pages in any way.
// Pages should be writable by user and kernel.
// Panic if any allocation attempt fails.
//


// mapira region memorije od va do va+len
// u okruzenju e
// algoritam je jednostavan

static void
region_alloc(struct Env *e, void *va, size_t len)
{
	// LAB 3: Your code here.
	// (But only if you need it for load_icode.)
	uintptr_t va_start = ROUNDDOWN( (uintptr_t)va , PGSIZE );
  uintptr_t va_end = ROUNDUP( (uintptr_t)va + len , PGSIZE );
  struct PageInfo * page;

  while( va_start != va_end ){
    page = page_alloc(0x0);
    if( page == NULL || page_insert( e->env_pgdir , page , (void *)va_start  , PTE_W | PTE_U | PTE_P ) )
      panic("region_alloc() : Problem with page allocation\n");
    ++page->pp_ref;
    va_start += PGSIZE;
  }
	// Hint: It is easier to use region_alloc if the caller can pass
	//   'va' and 'len' values that are not page-aligned.
	//   You should round va down, and round (va + len) up.
	//   (Watch out for corner-cases!)
}

//
// Set up the initial program binary, stack, and processor flags
// for a user process.
// This function is ONLY called during kernel initialization,
// before running the first user-mode environment.
//
// This function loads all loadable segments from the ELF binary image
// into the environment's user memory, starting at the appropriate
// virtual addresses indicated in the ELF program header.
// At the same time it clears to zero any portions of these segments
// that are marked in the program header as being mapped
// but not actually present in the ELF file - i.e., the program's bss section.
//
// All this is very similar to what our boot loader does, except the boot
// loader also needs to read the code from disk.  Take a look at
// boot/main.c to get ideas.
//
// Finally, this function maps one page for the program's initial stack.
//
// load_icode panics if it encounters problems.
//  - How might load_icode fail?  What might be wrong with the given input?
//

// funkcija koja ucitava program iz elf filea u VA prostor od procesa

static void
load_icode(struct Env *e, uint8_t *binary)
{
	// Hints:
	//  Load each program segment into virtual memory
	//  at the address specified in the ELF segment header.
	//  You should only load segments with ph->p_type == ELF_PROG_LOAD.
	//  Each segment's virtual address can be found in ph->p_va
	//  and its size in memory can be found in ph->p_memsz.
	//  The ph->p_filesz bytes from the ELF binary, starting at
	//  'binary + ph->p_offset', should be copied to virtual address
	//  ph->p_va.  Any remaining memory bytes should be cleared to zero.
	//  (The ELF header should have ph->p_filesz <= ph->p_memsz.)
	//  Use functions from the previous lab to allocate and map pages.
	//
	//  All page protection bits should be user read/write for now.
	//  ELF segments are not necessarily page-aligned, but you can
	//  assume for this function that no two segments will touch
	//  the same virtual page.
	//
	//  You may find a function like region_alloc useful.
	//
	//  Loading the segments is much simpler if you can move data
	//  directly into the virtual addresses stored in the ELF binary.
	//  So which page directory should be in force during
	//  this function?
	//
	//  You must also do something with the program's entry point,
	//  to make sure that the environment starts executing there.
	//  What?  (See env_run() and env_pop_tf() below.)

	// LAB 3: Your code here.

  
  struct Elf * elf_file = (struct Elf *)binary; // provjerimo da li se na pocetku binarnog fajla nalazi elf magic broj
  if( elf_file->e_magic != ELF_MAGIC ) // ako se ne nalazi, nismo dobili lokaciju pocetka elf filea
    panic("load_icode() : try to open not elf file\n");

  struct Proghdr * ph = ( struct Proghdr * )( binary + elf_file->e_phoff ); // pointer na pocetak prog headera 
  struct Proghdr * end_ph = ph + elf_file->e_phnum; // pointer na kraj proh headera
  // program header entry je fiksne velicine, e_phnum, je prog entry u tabeli

  lcr3( PADDR( e->env_pgdir  ) ); // postaviti VA na VA od procesa koji se ucitava

  for( ; ph < end_ph ; ++ph ){
    if( ph->p_type != ELF_PROG_LOAD ) // ako smo naisli na dio program koji se ne ucitava u RAM, preskociti ga
      continue;

    region_alloc( e , (void *)ph->p_va , ph->p_memsz ); // alocirati region fizicke memorije u kojem se treba mapirati dio programa i mapirati ga u VA od procesa
    memmove( (void *)( ph->p_va ) , (const void *)( binary + ph->p_offset ) , ph->p_filesz ); // mapirati kod od procesa
    memset( (void *)( ph->p_va + ph->p_filesz ) , 0x0 , ph->p_memsz - ph->p_filesz ); // ostatak popuniti sa 0, jer je to BSS
     // posto mapiramo u VA prostor koji se ne koristi, prakticno adresa ph->p_va nema mapiranje direktno
    // stoga da bi postavili bite, moram pronaci pa, svakog mapiranja, de uspostaviti korelaciju pa u KADDR te onda tek mapirati
    // naravno ovo je komplikovaniji pristup, tj. kada se ne bi mogao koristiti trik sa promjenom VA prostora sa lcr3()
   /* 
    uintptr_t dest = ph->p_va;
    uintptr_t src = (uintptr_t)( binary + ph->p_offset );
    uint32_t sz = ph->p_filesz;
    uint32_t copy_step;
    uintptr_t equ_addr;
    pte_t * pte;
   
    while( sz != 0 ){
      pte = pgdir_walk( e->env_pgdir , (const void*)dest , 0x0 );
      if( pte == NULL )
        panic("load_icode() : memory region is not found!\n");
      equ_addr = (uintptr_t)KADDR( *pte & 0xFFFFF000 ) + ( dest % PGSIZE );
      copy_step = PGSIZE - ( dest & 0x00000FFF ) ;
      copy_step = copy_step > sz ? sz : copy_step;
      memmove( (void *)equ_addr , (const void*)src , copy_step );
      src += copy_step;
      dest += copy_step;
      sz -= copy_step;
    }

    sz = ph->p_memsz - ph->p_filesz;
    while( sz != 0 ){
      pte = pgdir_walk( e->env_pgdir , (const void*)dest , 0x0 );
      if( pte == NULL )
        panic("load_icode() : memory region is not found!\n");
      equ_addr = (uintptr_t)KADDR( *pte & 0xFFFFF000 ) + ( dest % PGSIZE );
      copy_step = PGSIZE - ( dest & 0x00000FFF ) ;
      copy_step = copy_step > sz ? sz : copy_step;
      memset( (void *)equ_addr , 0x0 , copy_step );
      dest += copy_step;
      sz -= copy_step;    
    } 
   */
  
  }
   
  lcr3( PADDR( kern_pgdir ) ); // vratiti VA na VA prostor od kernela
  e->env_tf.tf_eip = ( (struct Elf*)(binary) )->e_entry; // postaviti eip od procesa na pocetak programa, adresa pocetka se nalazi u e_entry polju

	// Now map one page for the program's initial stack
	// at virtual address USTACKTOP - PGSIZE.
  //
	// LAB 3: Your code here.
  region_alloc(e,(void *)(USTACKTOP-PGSIZE),PGSIZE); // alocira se prostor za user stack
}



//
// Allocates a new env with env_alloc, loads the named elf
// binary into it with load_icode, and sets its env_type.
// This function is ONLY called during kernel initialization,
// before running the first user-mode environment.
// The new env's parent ID is set to 0.
//
void
env_create(uint8_t *binary, enum EnvType type)
{
	// LAB 3: Your code here.
	// If this is the file server (type == ENV_TYPE_FS) give it I/O privileges.
	// LAB 5: Your code here.
  
	struct Env * environment;
 	
  if( env_alloc( &environment , 0 ) )
    panic("env_create() : can not allocate space for environment\n");

  load_icode( environment , binary );
 	
  environment->env_type = type;
  
  if( type == ENV_TYPE_FS )
    environment->env_tf.tf_eflags |= FL_IOPL_3;
  
  cprintf("env_create() : lab4 code - set env_status = ENV_RUNNABLE\n");
  
  environment->env_status = ENV_RUNNABLE;

}

//
// Frees env e and all memory it uses.


// funkcija koja oslobadja resurse koje je okruzenje zauzelo
void
env_free(struct Env *e)
{
	pte_t *pt;
	uint32_t pdeno, pteno;
	physaddr_t pa;

	// If freeing the current environment, switch to kern_pgdir
	// before freeing the page directory, just in case the page
	// gets reused.

	if (e == curenv) // ako oslobadjamo resurse od okruzenja koje se trenutno izvrsava aka. unistavamo ga, posto cemo osloboditi i frame od 4KB
		lcr3(PADDR(kern_pgdir)); // koji je koriste za page directory , potrebno je da se prebacimo u virtuelni adresni prostor od kernela, jer poslije
                            // oslobadjanja stranica moze biti zauzeta od strane drugog procesa koji se izvrsava na drugoj jezgri i njen sadrzaj moze
                            // biti promjenjen, samim tim page directory ce biti invalidan

	// Note the environment's demise.
	// cprintf("[%08x] free env %08x\n", curenv ? curenv->env_id : 0, e->env_id);
  // poruka trenutni proces tj.  koji se izvsava( ako se ijedan izvrsava ), unistava proces ...

	// Flush all mapped pages in the user portion of the address space
	static_assert(UTOP % PTSIZE == 0); // generira compajlersku gresku ako UTOP nije djeljivo sa 4MB , tj user prostor ne pocinje od novog frame velicine 4MB


	for (pdeno = 0; pdeno < PDX(UTOP); pdeno++) {

		// only look at mapped page tables
		if (!(e->env_pgdir[pdeno] & PTE_P)) // ako PT od datog PDE ne postoji, tj nijedna adresa u bloku od 4MB se ne koristi, skociti na sljedecu iteraciju
			continue;

		// find the pa and va of the page table
		pa = PTE_ADDR(e->env_pgdir[pdeno]); // fizicka adresa PT od datog PDEA 
		pt = (pte_t*) KADDR(pa); // viruelna adresa / linearna adresa od PTa

		// unmap all PTEs in this page table
		for (pteno = 0; pteno <= PTX(~0); pteno++) { // vrtimo petlju kroz svaki PTE 
			if (pt[pteno] & PTE_P) // ako je mapiranje prisutno
				page_remove(e->env_pgdir, PGADDR(pdeno, pteno, 0)); // uklanjamo datu stranicu
		}

		// free the page table itself
		e->env_pgdir[pdeno] = 0; // da ne bi imali dangling pointer
		page_decref(pa2page(pa)); // dealociramo stranicu koja se koristi za PT od datog PDE
	}
	// free the page directory
	pa = PADDR(e->env_pgdir); // fizicka adresa PDa datog mapiranja
	e->env_pgdir = 0; // da ne bi imali dangling pointer 
	page_decref(pa2page(pa)); // dealociramo datu stranicu

	// return the environment to the free list
	e->env_status = ENV_FREE; // frame okruzenja se postavlja kao slobodan
	e->env_link = env_free_list; // i dodaje se na pocetak linkane liste
	env_free_list = e;
}

//
// Frees environment e.
// If e was the current env, then runs a new environment (and does not return
// to the caller).
//
void
env_destroy(struct Env *e)
{

	// If e is currently running on other CPUs, we change its state to
	// ENV_DYING. A zombie environment will be freed the next time
	// it traps to the kernel.
	if (e->env_status == ENV_RUNNING && curenv != e) {
		e->env_status = ENV_DYING;
		return;
	}

	env_free(e);

	if (curenv == e) {
		curenv = NULL;
		sched_yield();
	}
}


//
// Restores the register values in the Trapframe with the 'iret' instruction.
// This exits the kernel and starts executing some environment's code.
//
// This function does not return.


// niz instrukcija koje vrate u registre, sadrzaj virtuelnog procesa
// prvo se vrati esp i to na pocetak tf
// zatim se popaju opsti registri eax ecx edx eax itd.
// zatim se popaju es , ds
// zatim preskoci 8 bajti nak ojima su pohranjeni trap number i error code
// i zatim pomocu iret vrati se u zavisnosti od vrste iznimke ostali registri
// to su cs eip eflags + u nekim slucajevi ss esp ( kada je prekid dosao iz user moda ) 
void
env_pop_tf(struct Trapframe *tf)
{
	// Record the CPU we are running on for user-space debugging
	curenv->env_cpunum = cpunum();

	asm volatile(
		"\tmovl %0,%%esp\n"
		"\tpopal\n"
		"\tpopl %%es\n"
		"\tpopl %%ds\n"
		"\taddl $0x8,%%esp\n" /* skip tf_trapno and tf_errcode */
		"\tiret\n"
		: : "g" (tf) : "memory");
	panic("iret failed");  /* mostly to placate the compiler */
}

//
// Context switch from curenv to env e.
// Note: if this is the first call to env_run, curenv is NULL.
//
// This function does not return.
//
void
env_run(struct Env *e)
{
	// Step 1: If this is a context switch (a new environment is running):
	//	   1. Set the current environment (if any) back to
	//	      ENV_RUNNABLE if it is ENV_RUNNING (think about
	//	      what other states it can be in),
	//	   2. Set 'curenv' to the new environment,
	//	   3. Set its status to ENV_RUNNING,
	//	   4. Update its 'env_runs' counter,
	//	   5. Use lcr3() to switch to its address space.
	// Step 2: Use env_pop_tf() to restore the environment's
	//	   registers and drop into user mode in the
	//	   environment.

	// Hint: This function loads the new environment's state from
	//	e->env_tf.  Go back through the code you wrote above
	//	and make sure you have set the relevant parts of
	//	e->env_tf to sensible values.

	// LAB 3: Your code here
  
	if( curenv ){ 
    if(  curenv->env_status == ENV_RUNNING )
      curenv->env_status = ENV_RUNNABLE;
  }
     	curenv = e;
     	curenv->env_status = ENV_RUNNING;
     	++curenv->env_runs; // posto se nakon tretmana sistemskog poziva ponovo pozove data funkcija, moguce da ce broj pokretanja biti biti netacan
      lcr3( PADDR( curenv->env_pgdir ) );
      //cprintf( "Eflags je %08x\n" , curenv->env_tf.tf_eflags );
     // cprintf("\t\t\t\t\t\tJezgro %d otpuska kljuc\n",cpunum());
      unlock_kernel();
    	env_pop_tf( &curenv->env_tf ); // proces se pocne izvrsavati kada je na procesor postave njegovi registri iz virt procesora
}

