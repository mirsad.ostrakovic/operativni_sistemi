// Simple command-line kernel monitor useful for
// controlling the kernel and exploring the system interactively.

#include <inc/stdio.h>
#include <inc/string.h>
#include <inc/memlayout.h>
#include <inc/assert.h>
#include <inc/x86.h>

#include <kern/console.h>
#include <kern/monitor.h>
#include <kern/kdebug.h>
#include <kern/trap.h>

#include <inc/types.h>
#include <kern/pmap.h>

#define CMDBUF_SIZE	80	// enough for one VGA text line

int mon_showmappings(int, char**, struct Trapframe *);
int mon_set_mapping_permission(int, char**, struct Trapframe *);
int mon_debugger_ni(int, char**, struct Trapframe *);

struct Command {
	const char *name;
	const char *desc;
	// return -1 to force monitor to exit
	int (*func)(int argc, char** argv, struct Trapframe* tf);
};

static struct Command commands[] = {
	{ "help", "Display this list of commands", mon_help },
	{ "kerninfo", "Display information about the kernel", mon_kerninfo },
  { "backtrace", "Print stack backtrace", mon_backtrace },
  { "showmappings", "Show virtual address mappings", mon_showmappings },
  { "setmappingperm", "Set mapping permission" , mon_set_mapping_permission },
  { "si", "Debugger single step instruction" , mon_debugger_ni }
};





/***** Challeng 2 zadatak implementacija ******/

static unsigned int hex_to_int(char* hex, unsigned int* dec){
  if( hex[0] != '0' || !( hex[1] == 'x' || hex[1] == 'X' ) )
    return -1;
  *dec = 0;
  int i = 2;
  for( ; hex[i] != '\0' ; ++i ){
    if( hex[i] >= '0' && hex[i] <= '9' )
      *dec = ( *dec << 4 ) + ( hex[i] - '0' );
    else if( hex[i] >= 'A' && hex[i] <= 'F' )
      *dec = ( *dec << 4 ) + ( hex[i] - 'A' + 10 );
    else if( hex[i] >= 'a' && hex[i] <= 'f' )
      *dec = ( *dec << 4 ) + ( hex[i] - 'a' + 10 );
    else return -1; 
  }
  if( i > 10 ) return -1; // hex broj moze imati najvise 8 cifri inace konverzija ne radi
  return 0;
}


int mon_showmappings(int argc, char **argv, struct Trapframe *tf)
{
  physaddr_t pgdir_addr = rcr3();
  pde_t *pgdir = KADDR(pgdir_addr); // za trenutno mapiranje
  unsigned int start;
  unsigned int end; 
  if( argc != 3 || hex_to_int(argv[1], &start) || hex_to_int(argv[2], &end) ) return -1;
  start = ROUNDDOWN(start,PGSIZE);
  end = ROUNDUP(end,PGSIZE);
  pte_t * pte;
  for( ; start != end ; start += PGSIZE ){
    if( (pgdir[ PDX(start) ]  & PTE_P ) ==  PTE_P  && ( pgdir[ PDX(start) ] & PTE_PS ) == PTE_PS ){
      cprintf("%08x has mapping into %08x" , start , ( pgdir[ PDX(start) ] & 0XFFFFF000 ) + start % PTSIZE );
      cprintf(" %c %c %c\n" , pgdir[ PDX(start) ] & PTE_P ? 'P' : ' ' , pgdir[ PDX(start) ] & PTE_W ? 'W' : ' ' , pgdir[ PDX(start) ] & PTE_U ? 'U' : ' ' );
    }
    else{
      pte = pgdir_walk(pgdir,(void *)start,0x0);
      if( pte == NULL || !( *pte & PTE_P ) )
        cprintf("%08x has no mapping\n" , start);
      else{
        cprintf("%08x mapping into %08x " , start , *pte & 0XFFFFF000);
        cprintf(" %c %c %c\n" , *pte & PTE_P ? 'P' : ' ' , *pte & PTE_W ? 'W' : ' ' , *pte & PTE_U ? 'U' : ' ' );
      }
    }
  }
  return 0;
}

int set_permission( uint32_t * pde , char * permission ){
  int i = 0;
  if( permission[0]  == '!' ) ++i;
  switch( permission[i] ){
    case 'U':
    case 'u':
      *pde = i ? *pde & ~PTE_U : *pde | PTE_U;
      break;
    case 'W':
    case 'w':
      *pde = i ? *pde & ~PTE_W : *pde | PTE_W;
      break;
    case 'P':
    case 'p':
      *pde = i ? *pde & ~PTE_P : *pde | PTE_P;
    default:
      return -1;
  }
  return 0;
}


int mon_set_mapping_permission(int argc, char **argv, struct Trapframe *tf){
  pde_t *pgdir = (pde_t*)KADDR( rcr3() );
  unsigned int addr;
  if( hex_to_int(argv[1], &addr) ) return -1;
  pde_t * pde = (pde_t*)(&pgdir[ PDX(addr) ]);
  int ret_val = 0;
  if( *pde & PTE_P ){
    if( *pde & PTE_PS )
      ret_val = set_permission( pde , argv[2] );
    else{
      pte_t * pte = pgdir_walk(pgdir,(void *)addr,0x0);
      if( pte )
        ret_val = set_permission( pte , argv[2] ); 
      else
        cprintf("%08x has no mapping\n");
    }
  }
  else
    cprintf("%08x has no mapping \n"); 
  return ret_val;
}

/***** Implementations of basic kernel monitor commands *****/





int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(commands); i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
	return 0;
}

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}
// stack:
// arg5
// arg4
// arg3
// arg2
// arg1
// eip
//ebp-> old ebp


int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
    //extern unsigned int bootstacktop; // postoji u pmap.h
    struct Eipdebuginfo info;
    unsigned int* ebp = (unsigned int*) read_ebp();
    uintptr_t eip;
    while( ebp != 0 ){
      eip = *(ebp+1);
      cprintf("ebp %08x   eip %08x  args %08x %08x %08x %08x %08x\n",ebp,*(ebp+1),*(ebp+2),*(ebp+3),*(ebp+4),*(ebp+5),*(ebp+6));

      if( debuginfo_eip( eip , &info ) == 0 ){
        cprintf("%s:%d: ", info.eip_file , info.eip_line );
        for( uint32_t i = 0u ; i < info.eip_fn_namelen ; ++i )
          cprintf("%c",info.eip_fn_name[i]);
        cprintf("+%d\n" , eip - info.eip_fn_addr);
      }

      ebp = (unsigned int*)( *(ebp) );

    }
  /*
    while( ebp != (&bootstacktop - 2) ){
      
      cprintf("ebp %08x   eip %08x  args %08x %08x %08x %08x %08x\n",ebp,*(ebp+1),*(ebp+2),*(ebp+3),*(ebp+4),*(ebp+5),*(ebp+6));
      eip = *(ebp+1);
      
      if( debuginfo_eip( eip , &info ) == 0 ){
        cprintf("%s:%d: ", info.eip_file, *(ebp+1)-info.eip_fn_addr);
        for( uint32_t i = 0u ; i < info.eip_fn_namelen ; ++i )
          cprintf("%c",info.eip_fn_name[i]);
        cprintf("+%d\n",info.eip_line);
      } 
        
      ebp = (unsigned int *)( *(ebp));
    }
    
    cprintf("ebp %08x  eip %08x  args %08x %08x %08x %08x %08x\n",ebp,*(ebp+1),*(ebp+2),*(ebp+3),*(ebp+4),*(ebp+5),*(ebp+6));    
    if( debuginfo_eip( eip , &info ) == 0 ){
      cprintf("%s:%d: ", info.eip_file, *(ebp+1)-info.eip_fn_addr);
      for( uint32_t i = 0u ; i < info.eip_fn_namelen ; ++i )
        cprintf("%c",info.eip_fn_name[i]);
      cprintf("+%d\n",info.eip_line);
    }
  */
    
    return 0;
}

//debuginfo_eip();
//debuginfo_eip(uintptr_t addr, struct Eipdebuginfo *info)

/***** Kernel monitor command interpreter *****/

#define WHITESPACE "\t\r\n "
#define MAXARGS 16

static int
runcmd(char *buf, struct Trapframe *tf)
{
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
		if (*buf == 0)
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
	}
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < ARRAY_SIZE(commands); i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
	return 0;
}


void
monitor(struct Trapframe *tf)
{

	char *buf;
  
  if( tf == NULL || tf->tf_trapno != 1 ){

	  cprintf("Welcome to the JOS kernel monitor!\n");
	  cprintf("Type 'help' for a list of commands.\n");
  }
  
  if (tf != NULL && tf->tf_trapno != 1 )
		print_trapframe(tf);

	while (1) {
		buf = readline("K> ");

		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}

}

int mon_debugger_ni(int argc , char **argv, struct Trapframe *tf){
  
  extern void env_pop_tf(struct Trapframe*);
  
  struct Eipdebuginfo info;
  if( debuginfo_eip( tf->tf_eip , &info ) == 0 ){
    // ovaj kod ispod ce biti formatiran da prikazuje stanje svig registara, ali za sada samo ispisuje rsp i ebp
    cprintf("esp %08x   eip %08x\n" , tf->tf_esp , tf->tf_eip );
    // identican kod onome za mon_bactrace
    if( debuginfo_eip( tf->tf_eip , &info ) == 0 ){
      cprintf("%s:%d: ", info.eip_file , info.eip_line );
      for( uint32_t i = 0u ; i < info.eip_fn_namelen ; ++i )
        cprintf("%c",info.eip_fn_name[i]);
      cprintf("+%d\n" , tf->tf_eip - info.eip_fn_addr);

    }  
  
  }
  else
    cprintf("mon_debugger_ni() : instruction is not fount in stabs\n");


  tf->tf_eflags |= FL_TF;
  env_pop_tf(tf);
  return -1; // ako dodje u ovu liniju znaci da se desila neka neprevidjena greska
}
