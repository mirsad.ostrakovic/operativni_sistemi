#include <inc/assert.h>
#include <inc/x86.h>
#include <kern/spinlock.h>
#include <kern/env.h>
#include <kern/pmap.h>
#include <kern/monitor.h>

void sched_halt(void);

// Choose a user environment to run and run it.
void
sched_yield(void)
{
	struct Env *idle;

	// Implement simple round-robin scheduling.
	//
	// Search through 'envs' for an ENV_RUNNABLE environment in
	// circular fashion starting just after the env this CPU was
	// last running.  Switch to the first such environment found.
	//
	// If no envs are runnable, but the environment previously
	// running on this CPU is still ENV_RUNNING, it's okay to
	// choose that environment.
  //  
	// Never choose an environment that's currently running on
	// another CPU (env_status == ENV_RUNNING). If there are
	// no runnable environments, simply drop through to the code
	// below to halt the cpu.

	// LAB 4: Your code here.
  //cprintf("Scheduler start by env : %d\n", cpunum() );
  uint32_t end_index = curenv ? curenv - envs : NENV - 1 ;
  
  for( uint32_t i = ( end_index + 1 ) % NENV ; i != end_index ; i = ( i + 1 ) % NENV ){
//    cprintf( "i = %d\n" , i ); 
    if( envs[i].env_status == ENV_RUNNABLE ){
   //   cprintf("Pronadjeno RUNNABLE okruzenje na %d\n",i); 
      env_run( &envs[i] );
    }
  }
  //cprintf("NIje pronadjeno niti jedno RUNNABLE okruzenje\n");

  if( curenv && curenv->env_status == ENV_RUNNING )
    env_run( curenv );
  else if( !curenv && envs[end_index].env_status == ENV_RUNNABLE ) // posljedni slucaj je skoro pa i nemoguce
    env_run( &envs[end_index] ); // dakle moze se desiti samo ako ni jedno okruzenje se ne vrti na trenunom jezgru cpu, a jedino runnable okruzenje je posljednje u nizu
    // da nemam mogucnost stavljanja jezgra u zaustavljeno/zamrznuto/halt stanje, ne bi nikad ni imali ovaj slucaj, jer se prvi procesi koje CPU jezgra pokrenu/"pokupe" kada je njihov
    // curenv == NULL nalaze uvijek na pocetku niza envs, jer se envs dodjeljuju procesima od indexa 0,1,2 itd.
    // uslov da se ovo napravi, je da moramo imati mnogo okruzenja koja su u stanju cekanja na neki event, tako da ona popune niz envs do kraja
  //cprintf("Nema aktivno orkuzenja\n");
	// sched_halt never returns
	sched_halt();
}

// Halt this CPU when there is nothing to do. Wait until the
// timer interrupt wakes it up. This function never returns.
//
void
sched_halt(void) // ISKOMENTARISATI POSLIJE RAD FUNKCIJE
{
	int i;

	// For debugging and testing purposes, if there are no runnable
	// environments in the system, then drop into the kernel monitor.
	for (i = 0; i < NENV; i++) {
		if ((envs[i].env_status == ENV_RUNNABLE ||
		     envs[i].env_status == ENV_RUNNING ||
		     envs[i].env_status == ENV_DYING))
			break;
	}
	if (i == NENV) {
		cprintf("No runnable environments in the system!\n");
		while (1)
			monitor(NULL);
	}

	// Mark that no environment is running on this CPU
	curenv = NULL;
	lcr3(PADDR(kern_pgdir));

	// Mark that this CPU is in the HALT state, so that when
	// timer interupts come in, we know we should re-acquire the
	// big kernel lock
	xchg(&thiscpu->cpu_status, CPU_HALTED);

	// Release the big kernel lock as if we were "leaving" the kernel
	unlock_kernel();

	// Reset stack pointer, enable interrupts and then halt.
	asm volatile (
		"movl $0, %%ebp\n"
		"movl %0, %%esp\n"
		"pushl $0\n"
		"pushl $0\n"	
		"sti\n"
		"1:\n"
		"hlt\n"
		"jmp 1b\n"
	: : "a" (thiscpu->cpu_ts.ts_esp0));
}

