/* See COPYRIGHT for copyright information. */

#include <inc/assert.h>
#include <inc/trap.h>

#include <kern/picirq.h>


// Current IRQ mask.
// Initial IRQ mask has interrupt 2 enabled (for slave 8259A).
uint16_t irq_mask_8259A = 0xFFFF & ~(1<<IRQ_SLAVE); // maskiranje svih interrupta osim onih na irq2, da bi se omogucili inerrupti sa slave PICa
                                                    // sa tim da ce svi interrupti na slave PICu biti maskirani
static bool didinit;

// Prva stvar koju je potrebno odraditi prije koristenja PICa je inicalizirati ga:
// komanda za inicijalizaciju je 0x11:
// nakon ove komande PIC ceka na upis 3 bajta na data portu:
// - 1 bit je vector offset
// - 2 bit je bit koji indicira kako je povezan sa master/slaveom
// - 3 bit je dodatna informacija o okruzenju

// Zasto 0x11 ? to je 0x10 + 0x1
// 0x10 - initialization required
// 0x08 - level triggered (edge) mode
// 0x04 - call address interval 4(8)
// 0x02 - single (cascade) mode
// 0x01 - ICW4 (not) needed
// vrijednosti u zagradama su pristune kada biti nisu setovani

/* Initialize the 8259A interrupt controllers. */
void
pic_init(void)
{
	didinit = 1;

  // 0x20 - command port for master PIC
  // 0x21 - data port for master PIC

  // Kada se ne vrsi inicijalizacija PICa na datom portu se pristupa INTERRUPT MASKED REGISTRU
  // svaki bit predstavlja jednu IRQ liniju, ako je dati bit setovan na 1 - ta IRQ linija je maskirana
  // kada se maskira bit 2 (indexom) - maskiramo komplet slave PIC
  
	// mask all interrupts
	outb(IO_PIC1+1, 0xFF);
	outb(IO_PIC2+1, 0xFF);

	// Set up master (8259A-1)

	// ICW1:  0001g0hi
	//    g:  0 = edge triggering, 1 = level triggering
	//    h:  0 = cascaded PICs, 1 = master only
	//    i:  0 = no ICW4, 1 = ICW4 required
	outb(IO_PIC1, 0x11); // pokrece proces inicijalizacije sa ICW4 , u kaskadno modu, edge triggered pic

	// ICW2:  Vector offset
	outb(IO_PIC1+1, IRQ_OFFSET); // postavljamo IRQ_OFFSET 

	// ICW3:  bit mask of IR lines connected to slave PICs (master PIC),
	//        3-bit No of IR line at which slave connects to master(slave PIC).
	outb(IO_PIC1+1, 1<<IRQ_SLAVE); // informacija master PICu na koji je IRQ line spojen slave PIC

  // ZA ICW4 biti :
  // 0x01 - 8086/88 (MCS-80/85) mode
  // 0x02 - auto (normal) EOI
  // 0x08 buffered mode /slave
  // 0x0c buffered mode/master
  // 0x10 special fully nested (not)

	// ICW4:  000nbmap
	//    n:  1 = special fully nested mode
	//    b:  1 = buffered mode
	//    m:  0 = slave PIC, 1 = master PIC
	//	  (ignored when b is 0, as the master/slave role
	//	  can be hardwired).
	//    a:  1 = Automatic EOI mode
	//    p:  0 = MCS-80/85 mode, 1 = intel x86 mode
	outb(IO_PIC1+1, 0x3); // 8086 mode , auto EOI , not nested mode

	// Set up slave (8259A-2)
	outb(IO_PIC2, 0x11); // ICW1 - setup slave PIC
	outb(IO_PIC2+1, IRQ_OFFSET + 8);	// ICW2 - offset slave PICA = master_offset + 8
	outb(IO_PIC2+1, IRQ_SLAVE);		// ICW3 - kaskadni identitet slave PICa
	// NB Automatic EOI mode doesn't tend to work on the slave.
	// Linux source code says it's "to be investigated".
	outb(IO_PIC2+1, 0x01);			// ICW4 - x86 mode

  // PIC ima 2 interrupt status registra: ISR - In-Service Register , IRR -Interrupt Request Register
  // ISR - govori koji se interrupt trenutno servisira od strane procesora
  // IRR - govori koji interrupti su aktivni, tj. koji hardver zahjeva servisiranje od strane interrupt servise routine
  // U ovisnosti od IMR - interrupt masked registru, PIC ce proslijediti interrupt iz IRR u ISR tj. procesor
  // ISR3 i IRR mogu biti procitani preko OCW3 komande
  // bit 3 mora biti setovan prilikom slanja ove komande bilo na 0x20 ili 0xa0 port (master/slave commmand port)
  // 0x0a za citanje IRR , 0xb za citanje ISR
  // poslij slanja komande IRR i ISR se citaju sa DATA PORTA dakle 0x20 a ne 0x21


	// OCW3:  0ef01prs
	//   ef:  0x = NOP, 10 = clear specific mask, 11 = set specific mask
	//    p:  0 = no polling, 1 = polling mode
	//   rs:  0x = NOP, 10 = read IRR, 11 = read ISR
	outb(IO_PIC1, 0x68);             /* clear specific mask */ // 0x68 = 01101000  -- ef = 11 - set specific mask mode
	outb(IO_PIC1, 0x0a);             /* read IRR by default */
  // prema navedenom sa postavkama IMR i postavljanjem SPECIFIC MASK MODE, prilikom obrade bilo kojeg interrupta svi ostalo su maskirani
  // bez obzira na prijoritet
	outb(IO_PIC2, 0x68);               /* OCW3 */
	outb(IO_PIC2, 0x0a);               /* OCW3 */

	if (irq_mask_8259A != 0xFFFF) // ako maskom nisu svi maskirani, postaviti masku
		irq_setmask_8259A(irq_mask_8259A);
}

void
irq_setmask_8259A(uint16_t mask)
{
	int i;
	irq_mask_8259A = mask; // promjena maske
	if (!didinit) // ako nije uradjen proces inicijaliacije PICa vratiti se 
		return;
	outb(IO_PIC1+1, (char)mask); // maskiranje na master PICu
	outb(IO_PIC2+1, (char)(mask >> 8)); // maskiranje na slave PICu (gornjih 8 bita prestavlja maske za slave, gornjih 8 za master PIC)
	cprintf("enabled interrupts:");
	for (i = 0; i < 16; i++) // interrupti koji su ukljuceni, njihovi pripadajuci biti nisu maskirani
		if (~mask & (1<<i))
			cprintf(" %d", i);
	cprintf("\n");
}

