// Search for and parse the multiprocessor configuration table
// See http://developer.intel.com/design/pentium/datashts/24201606.pdf

#include <inc/types.h>
#include <inc/string.h>
#include <inc/memlayout.h>
#include <inc/x86.h>
#include <inc/mmu.h>
#include <inc/env.h>
#include <kern/cpu.h>
#include <kern/pmap.h>

struct CpuInfo cpus[NCPU];
struct CpuInfo *bootcpu;
int ismp;
int ncpu;

// Per-CPU kernel stacks
unsigned char percpu_kstacks[NCPU][KSTKSIZE]
__attribute__ ((aligned(PGSIZE)));
// zauzima se prostor kernel stackovi za 8 jegri, koliko maximalno podrzava JOS
// ako ima manje jezgri, pristori se nece koristiti
// svaki stack je velicine KSTKSIZE
// i percpu_kstacks je poravnat na PGSIZE


// See MultiProcessor Specification Version 1.[14]

// da bi MP floating pointer stuktura bila ispravna signature biti moraju biti _MP_, takodjer suma svih polja ukljucujuci checksum mora biti 0
// na nacin kako je to definisano funkcijom sum()
// phyaddr ako je == 0, to je indikator da MP configuration table ne postoji, samim ti platforma nije MP
// length je velician strukture u paragrafima, paragraf == 16 bajti
// specrev je indikator verzije MP floating pointer stukture, moze biti 1 za 1.1 ili 4 za 1.4 verziju
// checksum se postavi na vrijednost takvu da suma svih polja od 1 bajt  strukture MP bude 0
// type je indikator, koji ako je 0, konfiguraciona tabele je predefisana, ako nije 0, tada je indikator da se koristi neka konfiguraciona tabela
// koju je definiso sistem
// imcrp - ako jea najjaci bit setovan na 0, indicira da je PIC mod implementiran, inace je virtual wire mode implementiran (pogledati dokumenatciju za detaljno objasnjenje) 
// ostali biti su rezervisani za upotrebu u nekim od iducih izadanja INTEL procesora
struct mp {             // floating pointer [MP 4.1]
	uint8_t signature[4];           // "_MP_"
	physaddr_t physaddr;            // phys addr of MP config table
	uint8_t length;                 // 1
	uint8_t specrev;                // [14]
	uint8_t checksum;               // all bytes must add up to 0
	uint8_t type;                   // MP system config type
	uint8_t imcrp;
	uint8_t reserved[3];
} __attribute__((__packed__));

// za provjeru validnosti MP konfiguracione tabele vazi isto kao i za MP floating pointer stukture
// lenght - velicin base dijela konfiguracione tabele
// version ekvivalentno sa spec_rev
// product je string koji indicira seriju i proizvodjaca procesora, prvih 8 bita za proizvodjaca, ostalih 12 za seriju procesora
// oemtable i oemlength ( sta je OEM procitati u dokumentaciji ), ako je oemtable == 0 ili oemlength == 0, OEM tabela ne postoji
// broj stavki u extended dijelu tabele
// adresa LAPICa na kojoj ga vidi svako jezgro, svako jezgro ima svoj LAPIC
// xlength, je velicina extended dijela MP konfiguracione tabele, 0 indicira da taj dio ne postoji
// xchecksum, ovu vrijednost je potrebno sabrati sa svim poljima iz extened dijela konfiguracione tabele, ako je vrijednost == 0
// extended dio konfigracione tabele je ispravno definisan

struct mpconf {         // configuration table header [MP 4.2]
	uint8_t signature[4];           // "PCMP"
	uint16_t length;                // total table length
	uint8_t version;                // [14]
	uint8_t checksum;               // all bytes must add up to 0
	uint8_t product[20];            // product id
	physaddr_t oemtable;            // OEM table pointer
	uint16_t oemlength;             // OEM table length
	uint16_t entry;                 // entry count
	physaddr_t lapicaddr;           // address of local APIC
	uint16_t xlength;               // extended table length
	uint8_t xchecksum;              // extended table checksum
	uint8_t reserved;
	uint8_t entries[0];             // table entries
} __attribute__((__packed__));

// za features, bit_0 - flag za FLOATING POINT UNIT , bit_9 isPresentAndActiveLAPIC
struct mpproc {         // processor table entry [MP 4.3.1]
	uint8_t type;                   // entry type (0)
	uint8_t apicid;                 // local APIC id
	uint8_t version;                // local APIC version
	uint8_t flags;                  // CPU flags
	uint8_t signature[4];           // CPU signature
	uint32_t feature;               // feature flags from CPUID instruction 
	uint8_t reserved[8];
} __attribute__((__packed__));

// mpproc flags
#define MPPROC_BOOT 0x02                // This mpproc is the bootstrap processor

// Table entry types
#define MPPROC    0x00  // One per processor
#define MPBUS     0x01  // One per bus
#define MPIOAPIC  0x02  // One per I/O APIC
#define MPIOINTR  0x03  // One per bus interrupt source
#define MPLINTR   0x04  // One per system interrupt source

// funkcija koja sumira sve elmenente u strukturi, pointer na pocetak strukture je dat sa addr, a velicina strukture je data sa len
// i to na nacin da strukturu posmatra kao niz unsigned char-ova, te kao povratnu vrijednost vraca modul 256 od ove sume
// Ova funkcija se upotrebljava da li dati dio koda MP struktura, ako jeste, onda modul 256 od sume svih njenih polja mora biti 0
// ili drugacije suma svih njenih polja, pri cemu je polje 1 BAJT ili unsigned char, jednaka je 0
// ako je u dijelu stukture eksplicitno nalazi polje checksum, nije ga potrebno ponovo sabirati sa svim poljima kao u slucaju
// MP floating pointer stukture i MP base dijela konfiguracione tabele, medjutim extended checksum za extended dio konfiguracione
// tabele se nalazi u base dijelu konfiguracione tabele, pa ga je potebno implicitno dodati na sumu svih polja extended dijela mp config tabele

static uint8_t
sum(void *addr, int len)
{
	int i, sum;

	sum = 0;
	for (i = 0; i < len; i++)
		sum += ((uint8_t *)addr)[i];
	return sum;
}

// Look for an MP structure in the len bytes at physical address addr.

// funkcija koja u fizickom komadu memorije [a , a + len) trazi MP floating pointer stukturu
// ova struktura definisana je na specifican nacin:
// pocetak strukture nalazi se na adresi djeljivoj sa sizeof strukture
// na pocetku stukture nalaze se niz od 4 char-a == "_MP_"
// suma svim polja pomocu funkcije uint8_t sum(void *addr, int len) mora biti == 0
// ako je funkcija pronasla datu strukturu vratiti njenu adresu inace vraca NULL ptr
static struct mp *
mpsearch1(physaddr_t a, int len)
{
	struct mp *mp = KADDR(a), *end = KADDR(a + len); // funkcija prima fizicke adrese, stoga ih mi moramo konvertovati u virtuelne

	for (; mp < end; mp++)
		if (memcmp(mp->signature, "_MP_", 4) == 0 &&
		    sum(mp, sizeof(*mp)) == 0)
			return mp;
	return NULL;
}

// Search for the MP Floating Pointer Structure, which according to
// [MP 4] is in one of the following three locations:
// 1) in the first KB of the EBDA;
// 2) if there is no EBDA, in the last KB of system base memory;
// 3) in the BIOS ROM between 0xE0000 and 0xFFFFF.

// funkcija koja trazi MP floating pointer strukturu u dijelovima memorije koji se predodredjeni da bi se u njima mogla nalaziti
// ako MP floating pointer struktura ne pronadjem ni u jednom od ova 3 komada memorije, procesor/platforma nije MP
// ako je struktura pronadjena vraca pointer na nju, inace vraca NULL pointer

// POJASNJENJE: segmentna adresa se izracunava kao u REAL mogu = CS << 4 + offset
// posto je prilikom startanja CPU, on u REAL mogu operacije, BIOS segmentne postavlja u skladu sa REAL modom
// vrijednost segmenta, NE PREDSTAVLJA pointer na vrijednost u GDT tabeli
static struct mp *
mpsearch(void)
{
	uint8_t *bda;
	uint32_t p;
	struct mp *mp;

	static_assert(sizeof(*mp) == 16); // sizeof() od strukture po definicij mora biti umnozak 16 u bajtima, na x86 je struktura definisana da bude 16 bajti

	// The BIOS data area lives in 16-bit segment 0x40.
	bda = (uint8_t *) KADDR(0x40 << 4);

	// [MP 4] The 16-bit segment of the EBDA is in the two bytes
	// starting at byte 0x0E of the BDA.  0 if not present.
	if ((p = *(uint16_t *) (bda + 0x0E))) {
		p <<= 4;	// Translate from segment to PA
		if ((mp = mpsearch1(p, 1024))) // pretraga pod 1)
			return mp;
	} else {
		// The size of base memory, in KB is in the two bytes
		// starting at 0x13 of the BDA.
		p = *(uint16_t *) (bda + 0x13) * 1024;
		if ((mp = mpsearch1(p - 1024, 1024))) // pretraga pod 2)
			return mp;
	}
	return mpsearch1(0xF0000, 0x10000); // pretraga pod 3) , u 3) se kaze 0xE0000 , a ovdje je od 0xF0000, u dokumentaciji pise 0XF0000
}

// Search for an MP configuration table.  For now, don't accept the
// default configurations (physaddr == 0).
// Check for the correct signature, checksum, and version.

// ako je konfiguraciona tabela pronadjena i ispravno definisana, tada vraca pointer na nju kao povratnu vrijednost, inace vrati NULL
// takodjer ako je konfiguraciona tabele ispravno definisana preko prvog argumenta se vrati pointer na mp floating pointer strukturu
static struct mpconf *
mpconfig(struct mp **pmp)
{
	struct mpconf *conf;
	struct mp *mp;

	if ((mp = mpsearch()) == 0) // ako mp tabela nadjena vratiti NULL ptr
		return NULL;
	if (mp->physaddr == 0 || mp->type != 0) { // ako je fizicka adresa == 0, tada tabela ne postoji, ili ako je type == 0 tabela je predefinisana, inace svaka vrijednost razlicita od 0
		cprintf("SMP: Default configurations not implemented\n"); // indicira da je defaultna konfiguracija definisana od strane sistema
		return NULL;
	}
	conf = (struct mpconf *) KADDR(mp->physaddr);
	if (memcmp(conf, "PCMP", 4) != 0) { // ako prva 4 bajta nisu PCMP, ne radi se o valjanoj konfiguracionoj tabeli
		cprintf("SMP: Incorrect MP configuration table signature\n");
		return NULL;
	}
	if (sum(conf, conf->length) != 0) { // ako sum svih polja, pri cemu je polje 1 bajt,  nije jednaka 0 u base dijelu konfiguracione tabele, onda ona nije ispravno definisana
		cprintf("SMP: Bad MP configuration checksum\n");
		return NULL;
	}
	if (conf->version != 1 && conf->version != 4) { // verzije konfiguracione tabele mogu biti 1.1 ili 1.4, stoga ako nije jedna od ove dvije vratiti gresku
		cprintf("SMP: Unsupported MP version %d\n", conf->version);
		return NULL;
	}
	if ((sum((uint8_t *)conf + conf->length, conf->xlength) + conf->xchecksum) & 0xff) { // istu provjeru sa sumom polja moramo uraditi i za extended dio konfiguracione tabele
		cprintf("SMP: Bad MP configuration extended checksum\n"); // sa tim da se xchecksum polje ne nalazi u extended dijelu konfiguracione tabele vec u baseu, pa moram ekspicitno sabrati 
		return NULL; // sa njim pri provjeri
	}
	*pmp = mp;
	return conf;
}

void
mp_init(void)
{
	struct mp *mp;
	struct mpconf *conf;
	struct mpproc *proc;
	uint8_t *p;
	unsigned int i;

	bootcpu = &cpus[0]; // ako procesor nije boot, ona je prvo jezgro i jedino koje postoji, u slucajnu MP sistema, ova pointer ce se promjenit da pokazuje na boot jezgro

	if ((conf = mpconfig(&mp)) == 0) // ako konfiguraciona tabele ne postoji ili nije ispravno definisana, procesor nije MP i potrebno je napustiti funkciju
		return;
	ismp = 1; // postavlja vrijednost globalne varijable ismp na 1, koja indicira da se radi o MP sistemu
	lapicaddr = conf->lapicaddr; // u MP konfiguracionoj tabeli nalazi se polje lapicaddr, koje predstavlja fizicku adresu na kojoj svaki CPU moze pristupiti svom LAPICu 
  
  // u extended dijelu konfiguracione tabele nalaze se stukture razlicite velicine, koje opisuju razlicite dijelove CPU i platforme
  // na pocetku svake tabele nalazi se bajt koji predstavlja identifikator o kojoj se stukturi radi, 0 - za CPU , 1 - BUS , 2 - I/O APIC itd.
  // za x86 struktura koja opicuje CPU je 20 bajti, dok su ostale po 8 bajti
  // jos jedan zahtjev x86 platforme je da sve stukture budu sortirane po identifikatoru u uzlaznom redoslijedu

	for (p = conf->entries, i = 0; i < conf->entry; i++) {
		switch (*p) {
		case MPPROC:
			proc = (struct mpproc *)p;
			if (proc->flags & MPPROC_BOOT) // ako je jezgro boot, postaviti bootcpu pointer na njegovu CpuInfo stukuru
				bootcpu = &cpus[ncpu];
			if (ncpu < NCPU) {
				cpus[ncpu].cpu_id = ncpu; // id procesora ekvivalentan je njegovoj indexu u stukturu, u xv6 je jednak IDu LAPICa
        // default konfiguracija MP tabela garantuje sortiranost svih strukutra u uzlaznom redoslijedu i dodjelivanje lapic idovima vrijednosti 0,1,2,3 ...
				ncpu++; // ncpu je globalna varijabla, pa ce po defaultu biti incijalizirana na 0
			} else {
				cprintf("SMP: too many CPUs, CPU %d disabled\n", // ako procesor ima vise jezgri nego sto JOS podrzava, ta se jezgra nece koristiti
					proc->apicid);
			}
			p += sizeof(struct mpproc);
			continue;
		case MPBUS:
		case MPIOAPIC:
		case MPIOINTR:
		case MPLINTR:
			p += 8;
			continue; // ostala konfiguraciona polja se preskacu 
		default:
			cprintf("mpinit: unknown config type %x\n", *p); //ako postoji neko nelavidno konfiguraciono polje, ne radi se od MP procesoru jer MP konfiguraciona tabele
			ismp = 0; // nije ispravno definisana, u tom slucaju postaviti flag ismp na 0 i postaviti i = conf->entry, tako da ce pri provjeri uslova izaci iz petlje
			i = conf->entry;
		}
	}

	bootcpu->cpu_status = CPU_STARTED; // status boot CPU jezgre se postavlja na STARTED, jer ona upravo izvrsava ovaj kod

  if (!ismp) { // ako sistem nije multiprocesing
		// Didn't like what we found; fall back to no MP.
		ncpu = 1; // broj jezgri je jedno
		lapicaddr = 0; // lapicaddr se postavlja na 0, sto je indikator da ne postoji
		cprintf("SMP: configuration not found, SMP disabled\n");
		return;
	}
	cprintf("SMP: CPU %d found %d CPU(s)\n", bootcpu->cpu_id,  ncpu); // ispis poruke koliko je pronadjeno jezgri prilikom startanja procesora


  // pisanjem na PORT 0x22 , vrijednosti 0x70 odabiremo da modificramo registar 0x70
  // port 0x23 sluzi da citanje i pisanje u odabrani registar
  // po defaultnoj konfiguraciji NMI i interrupti sa PICa su podeseni da direktno dolaze na CPUu - bit sa indexom 0 je setovan na 0
  // setovanjem ovog bita na 1 - forsiramo ove signale da prodju kroz LAPIC (NMI), odnosno I/O APIC(PIC)
  // operacija se izvodi multiplexiranjem ovi ulaza na procesoru sa datim bitom IMCR

	if (mp->imcrp) {
		// [MP 3.2.6.1] If the hardware implements PIC mode,
		// switch to getting interrupts from the LAPIC.
		cprintf("SMP: Setting IMCR to switch from PIC mode to symmetric I/O mode\n");
		outb(0x22, 0x70);   // Select IMCR
		outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
	}
}
