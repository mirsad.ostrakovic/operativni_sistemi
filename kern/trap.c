#include <inc/mmu.h>
#include <inc/x86.h>
#include <inc/assert.h>
#include <inc/string.h>

#include <kern/pmap.h>
#include <kern/trap.h>
#include <kern/console.h>
#include <kern/monitor.h>
#include <kern/env.h>
#include <kern/syscall.h>
#include <kern/sched.h>
#include <kern/kclock.h>
#include <kern/picirq.h>
#include <kern/cpu.h>
#include <kern/spinlock.h>

// MAKROI I KONSTANTE ZA WRMSR
#define MSR_IA32_SYSENTER_CS (0x174)
#define MSR_IA32_SYSENTER_ESP (0x175)
#define MSR_IA32_SYSENTER_EIP (0x176)

#define WRMSR(MSR,HI,LO) __asm__ __volatile__("wrmsr " : /* nema izlaza */ : "c" (MSR) , "a" (LO) , "d" (HI) )


// TREBA DODATI U H
void divide_zero_handler(struct Trapframe *);
void general_protection_handler(struct Trapframe *);
void breakpoint_handler(struct Trapframe *);
void debugger_handler(struct Trapframe *);

// MAKROI I KONSTANTE ZA WRMSR
#define MSR_IA32_SYSENTER_CS (0x174)
#define MSR_IA32_SYSENTER_ESP (0x175)
#define MSR_IA32_SYSENTER_EIP (0x176)

#define WRMSR(MSR,HI,LO) __asm__ __volatile__("wrmsr " : /* nema izlaza */ : "c" (MSR) , "a" (LO) , "d" (HI) )


// TREBA DODATI U H
void divide_zero_handler(struct Trapframe *);
void general_protection_handler(struct Trapframe *);
void breakpoint_handler(struct Trapframe *);
void debugger_handler(struct Trapframe *);

static struct Taskstate ts;

/* For debugging, so print_trapframe can distinguish between printing
 * a saved trapframe and printing the current trapframe and print some
 * additional information in the latter case.
 */
static struct Trapframe *last_tf;

/* Interrupt descriptor table.  (Must be built at run time because
 * shifted function addresses can't be represented in relocation records.)
 */
struct Gatedesc idt[256] = { { 0 } };
struct Pseudodesc idt_pd = {
  sizeof(idt) - 1, (uint32_t) idt
};


static const char *trapname(int trapno) // funkcija koja za broj iznimke da njen naziv
{
  static const char * const excnames[] = {
    "Divide error",
    "Debug",
    "Non-Maskable Interrupt",
    "Breakpoint",
    "Overflow",
    "BOUND Range Exceeded",
    "Invalid Opcode",
    "Device Not Available",
    "Double Fault",
    "Coprocessor Segment Overrun",
    "Invalid TSS",
    "Segment Not Present",
    "Stack Fault",
    "General Protection",
    "Page Fault",
    "(unknown trap)",
    "x87 FPU Floating-Point Error",
    "Alignment Check",
    "Machine-Check",
    "SIMD Floating-Point Exception"
  };

  if (trapno < ARRAY_SIZE(excnames))
    return excnames[trapno];
  if (trapno == T_SYSCALL)
    return "System call";
  if (trapno >= IRQ_OFFSET && trapno < IRQ_OFFSET + 16)
    return "Hardware Interrupt";
  return "(unknown trap)";
}


  void
trap_init(void) // funkcija koja postavlja IDT
{
  extern struct Segdesc gdt[];
  extern uint32_t vectors[];
  // LAB 3: Your code here.

  // Eksplicitno sve gateove postavljano da su interrupt gateovi, sto znaci da prilikom tretmane bilo koje vrste iznimke
  // Nece se moci desiti ni jedna druga iznimka, osim onih koji su unmaskable

  for( unsigned int i = 0u ; i < 256 ; ++i ) // svi trapovi su postavljeni na interrupt gate, sto znaci da se prilikom tretmana bilo koje iznimke ne moze generirati noza iznimka
    SETGATE( idt[i] , 0 , GD_KT , vectors[i] , 0 ); // osim onih koji dodju na non-maskable interrupt input, vise o tome u dokumentaciji za x86

  SETGATE( idt[T_BRKPT] , 0 , GD_KT , vectors[T_BRKPT] , 3 ); // svi trapovi/intrupputi su sa RPL 0 osim syscall i breakpoint
  SETGATE( idt[T_SYSCALL] , 0 , GD_KT , vectors[T_SYSCALL] , 3 );


  // Per-CPU setup 
  trap_init_percpu();
}

// Initialize and load the per-CPU TSS and IDT
  void // inicijalizira TSS i ucitava IDT 
trap_init_percpu(void)
{
  // The example code here sets up the Task State Segment (TSS) and
  // the TSS descriptor for CPU 0. But it is incorrect if we are
  // running on other CPUs because each CPU has its own kernel stack.
  // Fix the code so that it works for all CPUs.
  //
  // Hints:
  //   - The macro "thiscpu" always refers to the current CPU's
  //     struct CpuInfo;
  //   - The ID of the current CPU is given by cpunum() or
  //     thiscpu->cpu_id;
  //   - Use "thiscpu->cpu_ts" as the TSS for the current CPU,
  //     rather than the global "ts" variable;
  //   - Use gdt[(GD_TSS0 >> 3) + i] for CPU i's TSS descriptor;
  //   - You mapped the per-CPU kernel stacks in mem_init_mp()
  //   - Initialize cpu_ts.ts_iomb to prevent unauthorized environments
  //     from doing IO (0 is not the correct value!)
  //
  // ltr sets a 'busy' flag in the TSS selector, so if you
  // accidentally load the same TSS on more than one CPU, you'll
  // get a triple fault.  If you set up an individual CPU's TSS
  // wrong, you may not get a fault until you try to return from
  // user space on that CPU.
  //
  // LAB 4: Your code here:
  
  extern const uint32_t _syscall_handler[]; // napomena, ovo je adresa pocetka funkcije _syscall_handler a ne varijabla

  thiscpu->cpu_ts.ts_esp0 = KSTACKTOP - ( KSTKSIZE + KSTKGAP ) * cpunum();
  thiscpu->cpu_ts.ts_ss0 = GD_KD;
  gdt[ ( GD_TSS0 >> 3 ) + cpunum() ] = SEG16( STS_T32A , (uint32_t)( &thiscpu->cpu_ts ) , sizeof( struct Taskstate ) - 1 , 0 );
  gdt[ ( GD_TSS0 >> 3 ) + cpunum() ].sd_s = 0;
  // Setup a TSS so that we get the right stack
  // when we trap to the kernel.
  // Postavljane model specific registara za fast syscall
  
  // wrmsr instrukcija upisuje u model specific registar sa brojem koji je specifirciran u %ecx, 64bitnu vrijedno hi:lo iz %edx : %eax
  // na pocetku ce biti denisian makro WRMSR koji ce generirati inline asembly za potavljanje model specific registara
  // mi ovdje menjamo registre iz kojih se citaju vrijednosti za fast system call instrukcije , tj sysenter  

  WRMSR( MSR_IA32_SYSENTER_CS , 0 ,  GD_KT );
  WRMSR( MSR_IA32_SYSENTER_ESP , 0 , thiscpu->cpu_ts.ts_esp0 );
  WRMSR( MSR_IA32_SYSENTER_EIP , 0 ,  _syscall_handler );

  // TSS se u X86 arhitektrui za task conext switch, medjutim JOS nalin na LINUX, koristi softversku implementaciju promjene procesa
  // medjutim svaka jezgra cpu mora imati minimalno jedan definisan TSS iz razloga jer je ESP0 i SS0 polja task switch segmenta
  // prilikom prelaska iz user u kernel mod, za postavljanje kernel stacka

  // ts.ts_esp0 = KSTACKTOP; // stack pointer za kernel stack
  // ts.ts_ss0 = GD_KD; // deskriptor je code kernel descriptor

  // Initialize the TSS slot of the gdt.
  //gdt[GD_TSS0 >> 3] = SEG16(STS_T32A, (uint32_t) (&ts),
  //				sizeof(struct Taskstate) - 1, 0);
  //gdt[GD_TSS0 >> 3].sd_s = 0;
  // u GDT treba postojati jedan posebni desktiptor koji se naziva TSS, base mu treba biti lokacija TSSa u memoriji sto je kod nas &ts, a velicina, velicina TSS
  // sto je sizof( struct TASKSTATE ) , dpl 0

  // Load the TSS selector (like other segment selectors, the
  // bottom three bits are special; we leave them 0)
  ltr( GD_TSS0 + (uint16_t)( cpunum() << 3 ) ); // ucitavanje tss desktiptora u tss segmenti registar

  // Load the IDT
  lidt(&idt_pd); // ucitavamo idt na isti nacin kao gdt, asembler instrukcija idt prima lokaciju u memoriji/adresu
  // sa koje treba procitati 6 bajtra od kojih prva dva predstavljaju velicinu tabele, a ostala 4 pointer pa pocetak idt aka. adresu pocetka idt
}

  void
print_trapframe(struct Trapframe *tf) // funkcija koja ispisuje informativno stanja registara nakon uhvacene iznimke
{
  cprintf("TRAP frame at %p from CPU %d\n", tf, cpunum());
  print_regs(&tf->tf_regs);
  cprintf("  es   0x----%04x\n", tf->tf_es);
  cprintf("  ds   0x----%04x\n", tf->tf_ds);
  cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
  // If this trap was a page fault that just happened
  // (so %cr2 is meaningful), print the faulting linear address.
  if (tf == last_tf && tf->tf_trapno == T_PGFLT)
    cprintf("  cr2  0x%08x\n", rcr2());
  cprintf("  err  0x%08x", tf->tf_err);
  // For page faults, print decoded fault error code:
  // U/K=fault occurred in user/kernel mode
  // W/R=a write/read caused the fault
  // PR=a protection violation caused the fault (NP=page not present).
  if (tf->tf_trapno == T_PGFLT)
    cprintf(" [%s, %s, %s]\n",
        tf->tf_err & 4 ? "user" : "kernel",
        tf->tf_err & 2 ? "write" : "read",
        tf->tf_err & 1 ? "protection" : "not-present");
  else
    cprintf("\n");
  cprintf("  eip  0x%08x\n", tf->tf_eip);
  cprintf("  cs   0x----%04x\n", tf->tf_cs);
  cprintf("  flag 0x%08x\n", tf->tf_eflags);
  if ((tf->tf_cs & 3) != 0) {
    cprintf("  esp  0x%08x\n", tf->tf_esp);
    cprintf("  ss   0x----%04x\n", tf->tf_ss);
  }
}

  void
print_regs(struct PushRegs *regs) // funkcija koja ipsisuje stanja opstih registara
{
  cprintf("  edi  0x%08x\n", regs->reg_edi);
  cprintf("  esi  0x%08x\n", regs->reg_esi);
  cprintf("  ebp  0x%08x\n", regs->reg_ebp);
  cprintf("  oesp 0x%08x\n", regs->reg_oesp);
  cprintf("  ebx  0x%08x\n", regs->reg_ebx);
  cprintf("  edx  0x%08x\n", regs->reg_edx);
  cprintf("  ecx  0x%08x\n", regs->reg_ecx);
  cprintf("  eax  0x%08x\n", regs->reg_eax);
}

  static void
trap_dispatch(struct Trapframe *tf)
{

  // Handle processor exceptions.
  // LAB 3: Your code here.


  // optimalnije bi bilo da imamo vektor pointera na funkcije handlere i da uz pomocu indexa prekida pronadjemo funkciju handler i nju pozovemo, efektivno bi imali 
  // O(1) vremensku ovisnost, ovako imamo O(velicina tabele), koja je konstanta pa dobijemo opet O(1) vremensku ovisnot ali sa daleko vecim koeficjentom
  switch( tf->tf_trapno ){

    case T_SYSCALL: // prvi case uvijek treba biti za sistemski poziv, jer se oni u sustini i najcesce desavaju, te trebaju biti prvi tretirani zbog brzine os-a
      tf->tf_regs.reg_eax = syscall( tf->tf_regs.reg_eax , tf->tf_regs.reg_edx , tf->tf_regs.reg_ecx , tf->tf_regs.reg_ebx , tf->tf_regs.reg_edi , tf->tf_regs.reg_esi ); 
      break;


    case IRQ_OFFSET + IRQ_SPURIOUS:
      cprintf("Spurious interrupt on irq 7\n");
      print_trapframe(tf);
      break;

  // Handle clock interrupts. Don't forget to acknowledge the
  // interrupt using lapic_eoi() before calling the scheduler!
  // LAB 4: Your code here.
    
    case IRQ_OFFSET + IRQ_TIMER:
      lapic_eoi();
      sched_yield();
      break;


	// Handle keyboard and serial interrupts.
	// LAB 5: Your code here.



    case T_DEBUG:
      debugger_handler( tf );
      break;

    case T_DIVIDE: // u sustini iznimka je prekid koji se desi opcenito jednom tijekom izvrsavanja aplikacije, i obicno ce je i zatvoriti, zbog toga sto se ovi prekidi javljaju
      // izuzetno rijetko, njihovi caseovi idu na kraj, izmedju sistemskog poziva i iznimki trebali bi doci hardverski prekidi 
      divide_zero_handler( tf );
      break;

    case T_BRKPT:
      breakpoint_handler( tf );
      break;

    case T_GPFLT:
      general_protection_handler( tf );
      break;

    case T_PGFLT:
      page_fault_handler( tf );
      break;


      // ako trap nije handliran niti u jednom casu, onda ispisati poruku da JOS ne zna trenutno handlirati dati prekid
    default :
      // Unexpected trap: The user process or the kernel has a bug.
      print_trapframe(tf);
      if (tf->tf_cs == GD_KT)
        panic("unhandled trap in kernel");
      else {
        env_destroy(curenv);
        return;
      }
  }
}

  void
trap(struct Trapframe *tf)
{
  // The environment may have set DF and some versions
  // of GCC rely on DF being clear
  asm volatile("cld" ::: "cc");

  // Halt the CPU if some other CPU has called panic()
  extern char *panicstr;
  if (panicstr)
    asm volatile("hlt");

  // Re-acqurie the big kernel lock if we were halted in
  // sched_yield()
  if (xchg(&thiscpu->cpu_status, CPU_STARTED) == CPU_HALTED){
    //cprintf("\t\t\t\t\t\tJezgro %d koje je bilo uspavano pokusava zakljucati kernel\n",cpunum());
    lock_kernel();
    //cprintf("\t\t\t\t\t\tJezgro %d koje je bilo uspavano je zakljucalo kernel\n",cpunum());
  }


  // Check that interrupts are disabled.  If this assertion
  // fails, DO NOT be tempted to fix it by inserting a "cli" in
  // the interrupt path.
  assert(!(read_eflags() & FL_IF)); // interupt moraju biti iskljuceni, to positemo tako sto gate postavimo da je interrupt kada postavljano idt

 // if( tf->tf_trapno != 1 ) // ne prikazivati poruku za single steping, jer izgleda ruzno
 //   cprintf("Incoming TRAP frame at %p\n", tf);

  if ((tf->tf_cs & 3) == 3) { // ako je trap iz user moda
    // Trapped from user mode.

    // Acquire the big kernel lock before doing any
    // serious kernel work.
    // LAB 4: Your code here.
//    cprintf("\t\t\t\t\t\tPrije zakljucavanja od strane jezgra %d\n",cpunum());
    lock_kernel();
//    cprintf("\t\t\t\t\t\tZaklucano od strane %d jezgra\n",cpunum());

    assert(curenv);

    // Garbage collect if current enviroment is a zombie
    if (curenv->env_status == ENV_DYING) {
      env_free(curenv);
      curenv = NULL;
      sched_yield();
    }

    // Copy trap frame (which is currently on the stack)
    // into 'curenv->env_tf', so that running the environment
    // will restart at the trap point.
    curenv->env_tf = *tf; // kopiramo trapframe sa stacka u strukturu environment koja cuva podatke o procesu
    // The trapframe on the stack should be ignored from here on.
    tf = &curenv->env_tf; // aplikacija od sad koristi trapframe koji se nalazi u stukturi environment
    // sto efektivno znaci da nakon zavrsetka tretmana iznimke cijeli stack se moze isprazniti, bez bojaznosti od gubitka informacija zbog prebrisavanja i sl.
    // sto znaci da nem u sustini i ne treba vise od jednog kernel stacka po jezgru CPUa, sa druge strane ovo moze biti manjkavost, jer nemam u strukturi prostor za
    // jos jedan trapframe, sto u sustini znaci da procesor ne moze biti prekinut dok izvrsava sistemski poziv, jer tada moramo imati dva trapa pohranjena na stack
    // u memoriji iz razloga sto se prekid moze desiti prije nego sto su svi dijelovi prvog trapa postavljeni
  }
  
  //if( (tf->tf_cs & 3 ) == 0 ) IZ NEKOG RAZLOGA INTERRUPTE SA AP CPUova prikazuje kao da su dosli iz kernela
  //{
  //  if( tf->tf_eflags & FL_IF )
  //    cprintf("Ukljucen je interrupt flag\n");
  //  cprintf("Trap dolazi iz KERNELA, broj trapa je %d\n",tf->tf_trapno);
  //  cprintf("Cs registar je %08x\n", tf->tf_cs );
  //  print_trapframe(tf);
  //}

  // Record that tf is the last real trapframe so
  // print_trapframe can print some additional information.
  last_tf = tf;

  // Dispatch based on what type of trap occurred
  trap_dispatch(tf); // funkcija koja predstavlja dispecersku sluzbu za prekide, treba odabrati odgovarajucu funkciju koja tretira prekid i pozvat je 

  // If we made it to this point, then no other environment was
  // scheduled, so we should return to the current environment
  // if doing so makes sense.
  if (curenv && curenv->env_status == ENV_RUNNING)
    env_run(curenv);
  else
    sched_yield();
}


// svi handleri za iznimke urade panic ako je iznimka dosla iz kernela
// ili zatvore proces ako je dosla iz user moda, tj dok se izvrsavao kod aplikacije
// i pri tome ispisu neku prigodnu poruku
  void
page_fault_handler(struct Trapframe *tf)
{
  uint32_t fault_va;

  // Read processor's CR2 register to find the faulting address
  fault_va = rcr2();

  // Handle kernel-mode page faults.

  // LAB 3: Your code here.
  if( (tf->tf_cs & 3) == 0 )
    panic("page_fault_handler() : page fault happened in kernel mode\n");

  // We've already handled kernel-mode exceptions, so if we get here,
  // the page fault happened in user mode.

  // Call the environment's page fault upcall, if one exists.  Set up a
  // page fault stack frame on the user exception stack (below
  // UXSTACKTOP), then branch to curenv->env_pgfault_upcall.
  //
  // The page fault upcall might cause another page fault, in which case
  // we branch to the page fault upcall recursively, pushing another
  // page fault stack frame on top of the user exception stack.
  //
  // It is convenient for our code which returns from a page fault
  // (lib/pfentry.S) to have one word of scratch space at the top of the
  // trap-time stack; it allows us to more easily restore the eip/esp. In
  // the non-recursive case, we don't have to worry about this because
  // the top of the regular user stack is free.  In the recursive case,
  // this means we have to leave an extra word between the current top of
  // the exception stack and the new stack frame because the exception
  // stack _is_ the trap-time stack.
  //
  // If there's no page fault upcall, the environment didn't allocate a
  // page for its exception stack or can't write to it, or the exception
  // stack overflows, then destroy the environment that caused the fault.
  // Note that the grade script assumes you will first check for the page
  // fault upcall and print the "user fault va" message below if there is
  // none.  The remaining three checks can be combined into a single test.
  //
  // Hints:
  //   user_mem_assert() and env_run() are useful here.
  //   To change what the user environment runs, modify 'curenv->env_tf'
  //   (the 'tf' variable points at 'curenv->env_tf').

  // LAB 4: Your code here.
  
  struct UTrapframe u_tf;
 
 // cprintf("Adresa env_pgfault rutine je: %d\n" , curenv->env_pgfault_upcall);
 // cprintf("UXSTACKTOP je : %08x\n",UXSTACKTOP);
  if( ! curenv->env_pgfault_upcall ){ // ne postoji handler funkcija sa user strane, za handliranje pgfaulta
    cprintf("[%08x] user fault va %08x ip %08x\n" , curenv->env_id , fault_va , tf->tf_eip );
    print_trapframe(tf);
    env_destroy(curenv);
  }
  // vrh user exception stacka, ako aplikacija prethodno nije nalazila na user exception stacku je, je UXSTACKTOP
  // inace, ako se esp aplikacije nalazio u regionu memoriju  [USTACKTOP,UXSTACKTOP) pri cemu je 
  // [UXSTACKTOP-PGSIZE,UXSTACKTOP) mapiran
  // [USTACKTOP,UXSTACKTOP-PGSIZE) nemapiran dio, da bi se provjeravao stack overflow
  // tada se vrh steka postavlja prijasnja lokacija esp umanjena za 4 bajta - 32 bita
  uintptr_t ux_stack_top = ( curenv->env_tf.tf_esp >= USTACKTOP && curenv->env_tf.tf_esp < UXSTACKTOP ) ? curenv->env_tf.tf_esp - 4 : UXSTACKTOP;
  uintptr_t ux_stack_bottom = ux_stack_top - sizeof( struct UTrapframe );
  
  // pomocu user_mem_assert cemo provjeriti da li je alociran prostor za exception stack, da li je omoguceno pisanje u njemu i da li se desio overflow
  // posto se handler sa kernel strane pisati u dio memorije [stack_bottom,stack_top+4) ovaj dio mora biti PTE_W , PTE_P, a posto ce ga koristi i user
  // handler mora biti i PTE_U
  // u slucaju da stranicca [UXSTACKTOP-PGSIZE,UXSTACKTOP) nije mapirana, ili je mapirana bez WRITE permisija pri prvom ulasku ce se
  // user_mem_assert aktivirati i unisti proces
  // u slucaju overflowa, ako overflow nije desio u user page handleru, on se moze desiti u kernelu, posto kernel mora postaviti strukturu
  // UTrapframe na UXSTACK, a ako je esp pokazuje pri dnu steka, struktura sama moze preci njegove granice, pa ce se desiti overflow
  // koji bi u kernelu izazvao kernel page fault i "oborio" JOS, to ce sprijeciti takodjer user_mem_assert
  // Sa druge strane ako se desio overlow pri user handlanje page_fault, mi ga necemo eksplictno tretirati, vec kada se dodje 
  // do user_mem_asserta, ako se desio overflow, kernel ce takodjer pokusati smjestiti struktura UTrapframe u isti taj dio koji nije mapiran
  // samim time ce biti sprijecen od user_mem_asserta i program ce se terminirati
  
  user_mem_assert( curenv , (const void *)( ux_stack_bottom ) , ux_stack_top - ux_stack_bottom , PTE_W | PTE_U | PTE_P );
  
  //strukturu cemo kopirati na vrh pointera na exception stack
  u_tf.utf_esp = curenv->env_tf.tf_esp;
  u_tf.utf_eflags = curenv->env_tf.tf_eflags;
  u_tf.utf_eip = curenv->env_tf.tf_eip;
  u_tf.utf_regs = curenv->env_tf.tf_regs;
  u_tf.utf_err = curenv->env_tf.tf_err;
  u_tf.utf_fault_va = fault_va;

  memmove( (void*)(ux_stack_bottom) ,(const void*)(&u_tf) , sizeof( struct UTrapframe ) ); // kopiramo stukturu u_tf na UXSTACK

  //postavljamo esp i eip tako da se mozemo nastaviti izvravanje u env_pgfault handleru sa user strane
  curenv->env_tf.tf_esp = ux_stack_bottom;
  curenv->env_tf.tf_eip = (uintptr_t)curenv->env_pgfault_upcall;
  env_run( curenv );

}


void
divide_zero_handler(struct Trapframe * tf){

  if( ( tf->tf_cs & 3 ) == 0 )
    panic("divide_zero_handler() : divide zero happened in kernel mode\n");

  print_trapframe(tf);
  env_destroy(curenv);

}


void
general_protection_handler(struct Trapframe * tf){
  if( ( tf->tf_cs & 3 ) == 0 )
    panic("general_protection_handler() : general protection fault happened in kernel mode\n");

  print_trapframe(tf);
  env_destroy(curenv);

}

void 
debugger_handler(struct Trapframe *tf){
  if( ( tf->tf_cs & 3 ) == 0 )
    panic("debugger_handler(): debugging exception came from kernel code\n");

  monitor(tf);
}


void
breakpoint_handler(struct Trapframe * tf){
  if( ( tf->tf_cs & 3 ) == 0 )
    panic("breakpoint_handler() : breakpoint set in kernel\n");

  // print_trapframe(tf); vec se u monitoru poziva

  monitor(tf);
}
