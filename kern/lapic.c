// The local APIC manages internal (non-I/O) interrupts.
// See Chapter 8 & Appendix C of Intel processor manual volume 3.

#include <inc/types.h>
#include <inc/memlayout.h>
#include <inc/trap.h>
#include <inc/mmu.h>
#include <inc/stdio.h>
#include <inc/x86.h>
#include <kern/pmap.h>
#include <kern/cpu.h>

// Local APIC registers, divided by 4 for use as uint32_t[] indices.
// Offset adrese se dijele sa 4 i na taj nacin se efektivno dobijaju indexi tih lokaciju u nizu int lapic_addr[]
#define ID      (0x0020/4)   // ID
#define VER     (0x0030/4)   // Version
#define TPR     (0x0080/4)   // Task Priority
#define EOI     (0x00B0/4)   // EOI
#define SVR     (0x00F0/4)   // Spurious Interrupt Vector
	#define ENABLE     0x00000100   // Unit Enable
#define ESR     (0x0280/4)   // Error Status
#define ICRLO   (0x0300/4)   // Interrupt Command
	#define INIT       0x00000500   // INIT/RESET
	#define STARTUP    0x00000600   // Startup IPI
	#define DELIVS     0x00001000   // Delivery status
	#define ASSERT     0x00004000   // Assert interrupt (vs deassert)
	#define DEASSERT   0x00000000
	#define LEVEL      0x00008000   // Level triggered
	#define BCAST      0x00080000   // Send to all APICs, including self.
	#define OTHERS     0x000C0000   // Send to all APICs, excluding self.
	#define BUSY       0x00001000
	#define FIXED      0x00000000
#define ICRHI   (0x0310/4)   // Interrupt Command [63:32]
#define TIMER   (0x0320/4)   // Local Vector Table 0 (TIMER)
	#define X1         0x0000000B   // divide counts by 1
	#define PERIODIC   0x00020000   // Periodic
#define PCINT   (0x0340/4)   // Performance Counter LVT
#define LINT0   (0x0350/4)   // Local Vector Table 1 (LINT0)
#define LINT1   (0x0360/4)   // Local Vector Table 2 (LINT1)
#define ERROR   (0x0370/4)   // Local Vector Table 3 (ERROR)
	#define MASKED     0x00010000   // Interrupt masked
#define TICR    (0x0380/4)   // Timer Initial Count
#define TCCR    (0x0390/4)   // Timer Current Count
#define TDCR    (0x03E0/4)   // Timer Divide Configuration

physaddr_t lapicaddr;        // Initialized in mpconfig.c
volatile uint32_t *lapic;

static void
lapicw(int index, int value)
{
	lapic[index] = value;
	lapic[ID];  // wait for write to finish, by reading
}

// LAPIC REGISTRI LINT0 i LINT1
// 7-0 bits - vector
// 10-8 bits - delivery mode
// 12 bit - delivery status
// 13 bit - interrupt input pin polarity
// 14 bit - remote IRR
// 15 bit - trigger mode
// 16 bit - masked
// ostali su rezervirani

// LAPIC TIMER REGISTAR
// 7-0 bits - vector
// 12 bit - delivery status
// 16 bit - masked
// 17 bit - timer mode
// ostali su rezervisani

// LAPIC SPURIOUS REGISTAR
// 7-0 bits - vector
// 8 bit - focus processor checking 
// 9 bit - APIC software enable/disable (1/0)


// ZA ostale:
// ERROR IMA bite 7-0 , 12 i 16
// PERFORMANCE MON. COUNTERS i THERMAL SENSOR imaju bite  7-0 , 10-8 , 12 i 16

// DELIVERY MODE
// 000 fixed
// 010 SMI
// 100 NMI
// 111 ExtINT
// 101 INIT

// TIMER MODE
// 0: one-shot
// 1: periodic

// MASK
// 0: not masked
// 1: masked

// DELIVERY STATUS
// 0: idle
// 1: send pending

// TRIGGER MODE
// 0: edge
// 1: level

void
lapic_init(void)
{
	if (!lapicaddr) // ako lapicaddr nije incijalizovala u mp_init, proces nije MP pa samim time nema lapic
		return;

	// lapicaddr is the physical address of the LAPIC's 4K MMIO
	// region.  Map it in to virtual memory so we can access it.
	lapic = mmio_map_region(lapicaddr, 4096);

  // U spurious interrupt vector registru nalazi se flag bit, koji je 9 bit indexom, a koji omogucava softversko ukljucivanje odnosno ukljucivanje
  // LAPICa, ako je bit na 1 LAPIC je ukljucen, u protivnom je iskljucen

	// Enable local APIC; set spurious interrupt vector.
	lapicw(SVR, ENABLE | (IRQ_OFFSET + IRQ_SPURIOUS));

  // timer oscilira frekvencijom sabirnice na koju je diretko prikljucen CPU, to je FSB bus
  // ova brzina je model specific velicina, obicno je oko 400 MHz za novije procesore, dok je za starije genracije u rasponu 50 do 250 MHz
  // U registru TDCR nalazi se broj koji definise djeljitelja frekvencije za timer, dakle ako je potrebno redukovati frekvenciju kojom
  // tajmer odbrojava od incijalne vrijednosti dati u TICR do 0, upisivanjem u TDCR razlicitih vrijednosti u bite 0 , 1 i 3
  // stimamo djetljitelja, tako da vrijednost 111 predstavlja - 0 , vrijednost 000 - 2 , vrijednost 001 - 4 itd. bit 2 je rezervisan i on uvijek treba biti 0
  
  // Recimo da je frekvencija FSB = 100MHz, pri trenutni postavkama, posto je djeljitelj postavljen na 1, i initial count na 10Mega, timer bi izazvao 10 interrupta po sec

	// The timer repeatedly counts down at bus frequency
	// from lapic[TICR] and then issues an interrupt.  
	// If we cared more about precise timekeeping,
	// TICR would be calibrated using an external time source.
  lapicw(TDCR, X1);
	lapicw(TIMER, PERIODIC | (IRQ_OFFSET + IRQ_TIMER));
	lapicw(TICR, 10000000);

	// Leave LINT0 of the BSP enabled so that it can get
	// interrupts from the 8259A chip.

  // PIC 8259 se veze za LINT0 ulaz samo BSP jezgre, dakle samo BSP jezgra ce opsluzivati interrupte
  // za ostale jezgre ce biti maskirani, tako sto ce se flag bit MASKET postaviti na 1

	// According to Intel MP Specification, the BIOS should initialize
	// BSP's local APIC in Virtual Wire Mode, in which 8259A's
	// INTR is virtually connected to BSP's LINTIN0. In this mode,
	// we do not need to program the IOAPIC.
	if (thiscpu != bootcpu)
		lapicw(LINT0, MASKED);

	// Disable NMI (LINT1) on all CPUs
	lapicw(LINT1, MASKED);


  // LAPIC VERSION registar
  // biti 7-0 - verzija
  // biti 23 - 16 - max lvt entry / broj LVT entryja - 1, sustinski prikazuje broj generacije intel procesora
  

	// Disable performance counter overflow interrupts
	// on machines that provide that interrupt entry.
	if (((lapic[VER]>>16) & 0xFF) >= 4) // sve genereacije polsije 4. ukljucujuci i nju imaju integriran u LAPIC intel performance counter registart
		lapicw(PCINT, MASKED);

	// Map error interrupt to IRQ_ERROR.
	lapicw(ERROR, IRQ_OFFSET + IRQ_ERROR);

  // 2x uzastopnim pisanjem u error status register, uzrokuje restartovanje svih njegovim flagova aka. postavlja mu se vrijednost na 0
	// Clear error status register (requires back-to-back writes).
	lapicw(ESR, 0);
	lapicw(ESR, 0);

  // upisivanje 0 u EOI registar, oznacava end of interrupt, upisivanje svake druge vrijednost se proizvesti GP fault
	// Ack any outstanding interrupts.
	lapicw(EOI, 0);

	// Send an Init Level De-Assert to synchronize arbitration ID's.
  
  // sekvenca komandi postavlja vrijednost arbitration ID na vrijednost LAPIC IDa
  // slanje ovakvog signala vec je predefinisano, pogledati u Intel Manualu
	lapicw(ICRHI, 0);
	lapicw(ICRLO, BCAST | INIT | LEVEL);
	while(lapic[ICRLO] & DELIVS) // provjerava delivery status, sve dok je 1 tj true, to znaci da komanda jos nije izvrsena (send pending)
		; // cim se komanda izvrsi, bit se setuje na 0 - tj prelazi u stanje idel, sto znaci da je komanda izvrsena i mozemo izaci iz petlje

	// Enable interrupts on the APIC (but not on the processor).
	lapicw(TPR, 0);
  // BITI U TASK PRIORITY registura oznacavaju minimalni broj kojeg vektor interrupta mora biti da bi ga LAPIC mogao zaprimiti
  // postavljanjem na 0, znaci da je LAPIC spreman prihvatiti interrupt bilo kojeg prioriteta
}

int
cpunum(void)
{
	if (lapic)
		return lapic[ID] >> 24;
	return 0;
}

// Acknowledge interrupt.
void
lapic_eoi(void)
{
	if (lapic)
		lapicw(EOI, 0);
}

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
static void
microdelay(int us)
{
}

#define IO_RTC  0x70

// CMOS je mala staticka memorije male potrosnje koji boravi na istom cipu kao i RTC - real time clock
// CMOSu  i RTC se moze pristupiti na portovima 0x70 i 0x71
// zadatak CMOSA je da cuva 50( ili 114 )bajti "setup" informacija za BIOS, dok je kompjuter iskljucen
// ovaj cip ima svoju posebnu bateriju za napajanje, pa tako on ostaje uvijek ukljucen tj. pod napajanjem
// registri sa adresama na 0x0 - 0xE, su predefinisani od strane CLOCK HARDVERA i svi moraju tu definiciju postovati
// ---  0xF adresa - IBM RESTART CODE ---
// 0x00 - 0x03 perform power-on restart
//  0x00 - software or unexpected restart
//  0x01 - restart after memory size check in real/vitrual mode
//  0x02 - restart after successful memory test in real/virtual mode
//  0x03 - restart after failed memory test in real/virtual mode
//  0x04 - INT 0x19 reboot
//  0x05 - flust keyboard ( EOI ) and jump on 0x40:0x0067
//  ... ( za ostale procitati dokumentaciju : https://bochs.sourceforge.net/techspec/CMOS-reference.txt )
//  0xA -resume execution by jump via 0x40:0x0067
// ... ( takodjer pogledati doumentaciju na linku gore )
// 0xD - 0xFF - perform power-on restart 

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapic_startap(uint8_t apicid, uint32_t addr)
{
	int i;
	uint16_t *wrv;

  //Posto se sva jezgrea startaju u REAL modu / 16 bitnom modu,
  //segmentno adresiranje se vrsi na nacin da se cs << 4 i na to sabere offset
  //mi u skladu sa tim postavljamo adrese u warm reset vector

	// "The BSP must initialize CMOS shutdown code to 0AH
	// and the warm reset vector (DWORD based at 40:67) to point at
	// the AP startup code prior to the [universal startup algorithm]."
	outb(IO_RTC, 0xF);  // offset 0xF is shutdown code
	outb(IO_RTC+1, 0x0A);
	wrv = (uint16_t *)KADDR((0x40 << 4 | 0x67));  // Warm reset vector
	wrv[0] = 0;
	wrv[1] = addr >> 4;

  // U LAPICu ICR registar je difinisan kao
  // 7-0 bits - vector
  // 10-8 bits - delivery mode
  // 11 bit - destination mode ( 1/0 - logical/physical )
  // 12 bit - delivery status
  // 14 bit - level ( 1/0  - assert/de-assert )
  // 15 bit - trigger mode ( 1/0 - level/edge )
  // 19-18 bits - destination shorthand( 00 - no shorthand , 01 - self , 10 - all including self , 11 - all excluding self )
  // 63-56 bits - destination field
  
  // warm restart je dodatk koji ima svaki standardni PC/AT BIOS  i omogucava da se INIT signal setuje - stvari u aktivno
  // stanje, bez zahtjevanja/uzrokovanja da procesor prodje kroz cjelu fazu inicijalizacije BIOSa
  // koristi se npr. da bi se procesor 80286 vratio u real mode
  // Procedura koja se prodje pri warm restartu je sljedeca:
  // - procita se shutdown code i CMOS rama na adresi 0xF, u nasem slucaju kod ce biti 0xA, sto indicira warm-restart
  // - kada POST( BIOS initialization procedure ) procita kod 0xA, tada izvrsi indirekti jump sa warm-reset vektorom
  // - koji predstavlja pointer u real modu - 2 bajtni, koji se nalazi na lokaciji 0x40:0x67
  // OVA PROCEDURA VRIJEDI ZA INIT IPI
  
  
	// "Universal startup algorithm."
	// Send INIT (level-triggered) interrupt to reset other CPU.
	lapicw(ICRHI, apicid << 24);
  // DESTIONATION FIELD = apicid
	lapicw(ICRLO, INIT | LEVEL | ASSERT);
  // DELIVERY MODE - 0x101 - INIT ( noviji intel procesori zahtjevaju da vector = 0x0 za ovaj delivery mode )
  // TRIGGER MODE - 0x1 - LEVEL
	// LEVEL MODE - 0x1 - ASSERTED
  microdelay(200);
	lapicw(ICRLO, INIT | LEVEL);
  // kod sljanja INIT IPI, OS mora prvo uraditi assert a, and de-assert delivery moda

	microdelay(100);    // should be 10ms, but too slow in Bochs!

  // DO SADA JE PROCESOR POSTAVLJEN DA MU PRVA SLJEDECA ISNTRUKCIJA BUDE ONA na koju pokazuje warm pointe pointer
  // WARM-VECTOR restart, medjutim on je jos uvije zakocen

  // STARTUP IPI uzrokuje da se procesor pocne izvrsavati u real modu sa pocetkom na adresi
  // 0x000VV000 gdje je VV - vector ( 8 bita ) koje su dio IPI poruke
  // Vektori A0 - BF su rezervisani i ne koriste se !!
  // STARTUP IPI nije moguce maskirati, ne uzrokuje promjenu stanja procesora osim EIPa
  // i moze biti izvrsena samo jednom priliko restartovanja ili poslije odmah poslije primitka INIT IPI
  // ne zahtjeva ni da ciljani APIC bude ukljucen, ako je procesor u zakocenom/halted stanju
  // uzrokovat ce napustanje tog stanja i prelazak u executing mode CS:IP = 0xVV00:0x0000
  

	// Send startup IPI (twice!) to enter code.
	// Regular hardware is supposed to only accept a STARTUP
	// when it is in the halted state due to an INIT.  So the second
	// should be ignored, but it is part of the official Intel algorithm.
	// Bochs complains about the second one.  Too bad for Bochs.
	for (i = 0; i < 2; i++) {
		lapicw(ICRHI, apicid << 24);
		lapicw(ICRLO, STARTUP | (addr >> 12));
		microdelay(200);
	}
}

void
lapic_ipi(int vector)
{
	lapicw(ICRLO, OTHERS | FIXED | vector); // dostavlja svim ostalim LAPICIMA interrupt specifiran u vector polju
	while (lapic[ICRLO] & DELIVS) // ceka se dok indicirajuci bit ne postane idle, sto znaci da je naredna proslijedjena
		;
}
