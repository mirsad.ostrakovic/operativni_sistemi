#include<iostream>
#define SZ_4MB 0x00400000
#define SZ_4KB 0x00001000

#define REGION_ADDR( REG_SZ , IND )  ( REG_SZ / rounddown_power2( IND ) * ( IND % rounddown_power2( IND ) )  )
  // formula za izicku adresu (buddy->sz_of_chunk / roundup_power2(index+1) * 2) * ( index % ( roundup_power2(index+1) / 2 ) );

struct ChunkOfMemory;
struct MemoryRegion;
struct PageInfo;

void allocation_MemoryRegion( struct MemoryRegion * , unsigned int , unsigned int );
bool mem_alloc( MemoryRegion *, std::size_t , PageInfo *);
unsigned int rounddown_power2( unsigned int );
void mem_free( PageInfo * );
//Memoriju cemo izdjeliti u regione od 4MB koji ce biti najveci komadi memorije koji se mogu alocirati odjednom
//Svaki taj region ce se moci dalje sitniti na komadice najmanje velicine od po 4KB
//Informacija o tom mapiranju regiona od 4MB u manje komadice  ce biti pohranjena u formi slicnoj HEAPu
//Indexi 2^N i 2^(N+1) -1 predstavljati red sa N-tim indexom u binarnom stablu
//Svaki element u nekom redu sadrzi informacije da li je on podjeljem na manje komdacie isSplited
//Svaki element u nekom redu sadrzi informacije da li je on vec alociran
//Dio memorije koji je podjeljen ne moze biti alociran
//Dio memorije koji je alociran ne moze biti podjeljen na manje komade
//Dio koji nije alociran i nije podjeljen moze se koristiti za alociranje komada memorije velicine tog komada
//Dio koji nije alociran i nije podjeljen moze se podjeliti da bi se alocirali manje komadi memorije

struct Memory{
  struct MemoryRegion* memory_;
  unsigned int sz_;
};

struct MemoryRegion{ 
  struct ChunkOfMemory * list_;
  unsigned int sz_;
  unsigned int sz_of_region_;
};

//Podaci o komadu memorije
//Velicina komada zavisit ce od njegove pozicije u list_ u MemoryRegion, opcenito veci index znaci da predstavlja manji komad memorije

struct ChunkOfMemory{
  bool isAllocated;
  bool isSplited;
  unsigned short ref_count;
};

//Da bi znali sve podatke o datom komadu memorije, potrebno je znati njegov region i index u tom regionu
//Ako je gore navedemo poznato mozemo znati i njegov fiziku adresu i samu velicinu

struct PageInfo{
  MemoryRegion * mem_region_;
  unsigned int index_in_region_;
};


void allocation_Memory( struct Memory* mem , unsigned int mem_sz ){
  mem->sz_ = mem_sz / SZ_4MB;
  if( mem_sz % SZ_4MB ) // kada prekrijemo cjelu memoriju sa komadima od po 4MB ostane jedan manji komadic njega cemo mapirati na sami kraj u jedan region
    ++mem_sz;
  mem->memory_ = new MemoryRegion[mem->sz_];
}
// Alociramo memoriju za struktura koja ce predstavljati memoriju
//

void init_Memory( struct Memory* mem , unsigned int mem_sz ){
  if( mem_sz % SZ_4MB ) 
    --mem->sz_;

  for( int i = 0 ; i < mem->sz_ ; ++i )
    allocation_MemoryRegion( &mem->memory_[i] , SZ_4MB , SZ_4KB );

  if( mem_sz % SZ_4MB ){
    ++mem->sz_;
    allocation_MemoryRegion( &mem->memory_[ mem->sz_ - 1 ] , rounddown_power2(mem_sz % SZ_4MB) , SZ_4KB );
  } 

}

bool malloc( Memory * mem , unsigned int sz , PageInfo * pg_info ){
  for( unsigned int i = 0u ; i < mem->sz_ ; ++i ){
    if( mem_alloc( &mem->memory_[i] , sz , pg_info ) )
      return true;
  }
  return false;
}

void free( PageInfo* pg_info ){
  mem_free(pg_info); 
}

void allocation_MemoryRegion( struct MemoryRegion * mem_region , unsigned int reg_sz , unsigned int min_chunk_sz ){ // uslov je da oba reg_sz i min_chunk_sz moraju biti 2^N 
  mem_region->sz_ = reg_sz/min_chunk_sz * 2; //B stablo za pohranu strukture sa n indexa treba mu 2^n redova, imat cemo ovdje za 4mb i 4kb , 11 redova
  mem_region->list_ = new ChunkOfMemory[ mem_region->sz_ ];
  mem_region->sz_of_region_ = reg_sz;
}

//U sutini svaki red u stablu ( koji je pohranjen u strukturi heap ) predstavlja region izdjeljen za komadice razlicite velicine
//U prvom redu imamo cjeli region recimo 4MB
//U drugom redu imamo 2 komadica od po 2MB
//U trecem redu imamo 4 komdacia od po 1MB itd.

void init_MemoryRegion( struct MemoryRegion * mem_region ){
  for( int i = 0 ; i < mem_region->sz_ ; ++i ){
    mem_region->list_[i].isAllocated = false;
    mem_region->list_[i].isSplited = false;
    mem_region->list_[i].ref_count = 0;
  }
}

unsigned int pow2( unsigned int n ){
  return 0x1 << n; 
}

unsigned int roundup_power2( unsigned int v ){
  v--;
  v |= v >> 1;
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
  v++;
  return v;
}

unsigned int rounddown_power2( unsigned int v ){
  return roundup_power2( v + 1) / 2; // zasto v + 1, da bi 4 se moglo zaokruziti na 8 kod roundup, dakle granicni slucaj
}

unsigned int alloc_chunk( MemoryRegion* mem_region , unsigned int index , unsigned int sz  ){
  if( sz == mem_region->sz_of_region_ /rounddown_power2(index) ){
    if( mem_region->list_[index].isSplited || mem_region->list_[index].isAllocated ) // ako je komad odgovarajuce velicine zauzet ili podjeljen vratiti 0u
      return 0u; // 0u je odabrana kao ret_val jer se u lisi prvi element tj element sa indexom 0 ne koristi
    else{
      mem_region->list_[index].isAllocated = true; //
      return index; // ako komadic nije niti podjeljen niti zauzet allcoiramo ga i vratitimo njegov index
    }
  }
  else{ // ako je potreban manji komadici onda provjeriti da li ga mozemo alocirati u lijevom ili desno podstablu datog cvora
    if( mem_region->list_[index].isAllocated ) return 0u; // ako je komadic zauzet, ne mozemo alocirati njegove manje komadice u njemu
    unsigned int r_val = alloc_chunk(mem_region, index*2 ,sz); // pokusamo zauzeti komadic u desnom podstablu
    if( r_val ){ // ako je lokacija uspjesna r_val ce biti index tog komadica u strukturi inace ce biti 0u
      mem_region->list_[index].isSplited = true; //ako je alokacija upsjela sigurno je dati komad  podjeljen, bez obzira da li bilo ili ne bilo podjeljeno prije
      return r_val; // vratimo index
    }
    r_val = alloc_chunk(mem_region, index*2 + 1, sz); // ako je alokacija neupsjena u lijevom, pokusamo u desnom podstablu
    if( r_val )
      mem_region->list_[index].isSplited = true; // ako je alokacija uspjesna oznacimo dati komadic kao podjeljen jer smo alocirali samo jedan njegov dio
    return r_val;
  }
}


bool mem_alloc( MemoryRegion * mem_region , std::size_t sz , PageInfo * pg_info ){

  unsigned int index = alloc_chunk(mem_region, 1u, sz); // 1u jer je najveci komadic na tom indexu
  if( index == 0 ) return false; // ako alokacija ne uspije vratiti false, inace postaviti strukturu Pageinfo
  pg_info->mem_region_ = mem_region;
  pg_info->index_in_region_ = index;
  return true;
}

void unite_chunk( ChunkOfMemory* list , unsigned int index ){
  if( !index )
    return;
  if( list[2*index].isAllocated || list[2*index].isSplited ) // ako lijevi pod komadic nije slobodan i nije jedinstven ne mozemo ujediniti 
    return;
  if( list[2*index+1].isAllocated || list[2*index+1].isSplited ) // isto vrijedi i za desni podkomadic, ako nije jedinstven(nedpojeljen) i slobodan ne mozemo vrsiti ujedinjavanje
    return;
  list[index].isSplited = false; // uradimo ujedinjavanje

  unite_chunk(list,index/2); // pokusamo ujediniti dati nononastali komadici sa buddy u komadic vece velicine
}

void mem_free( PageInfo * pg_info ){
  pg_info->mem_region_->list_[ pg_info->index_in_region_ ].isAllocated = false; // oznacimo dati region kao slobodan
  unite_chunk( pg_info->mem_region_->list_ , pg_info->index_in_region_ / 2 ); // pokusamo objediniti dati region sa drugim susjednim regionima

}


void print_region_rec( MemoryRegion * mem_region , unsigned int index ){
  if( index >= mem_region->sz_ ) return; // Ako indexiramo vam niza koji predstavlja BS u formi slicnoj HEAPU vrati se
  if( mem_region->list_[index].isSplited ){ // Ako je komad izdjeljen na manje komadice, za njih provjeri dostupnost
    print_region_rec( mem_region , 2*index );
    print_region_rec( mem_region , 2*index +1 );
  }
  else{
    std::cout <<  ( mem_region->sz_of_region_ / rounddown_power2(index) ); // velicina komadica
    if( mem_region->list_[index].isAllocated ) // ako je alociran printaj A, ako je slobodan printaj F 
      std::cout << "A ";
    else
      std::cout << "F ";
  }
}



void print_region( MemoryRegion* mem_region ){
  print_region_rec( mem_region , 1u ); 
}

void print_memory( Memory * mem ){
  for( unsigned int i = 0u ; i < mem->sz_ ; ++i ){
    std::cout << 4u * i << " MB - ";
    print_region( &mem->memory_[i] );
    std::cout << std::endl;
  } 
  std::cout << std::endl; 
}

unsigned int region_addr_to_index( unsigned int reg_sz , unsigned int addr , unsigned int sz ){
  // sz power of 2
  // addr + sz moraju odgovarat jednom komadicu u regionu
  return addr % sz ||  addr + sz > reg_sz ? 0u : reg_sz / sz + addr / sz;

}


void set_split_in_reg( MemoryRegion * mem_reg , unsigned int index ){
  if( index == 0u ) return;
  mem_reg->list_[index].isSplited = true;
  set_split_in_reg( mem_reg , index / 2 );
}

void mark_alloc_reg( struct MemoryRegion * mem_reg ,  unsigned int start , unsigned int end ){ // start i end moraju biti djeljivi sa 4KB 
  while( start != end ){
    unsigned int index = region_addr_to_index( mem_reg->sz_of_region_ , start , SZ_4KB );
    mem_reg->list_[index].isAllocated = true;
    set_split_in_reg( mem_reg , index / 2u );
    start += SZ_4KB;
  }
}
// Ideja je funkcije da za svaki region mapiramo dio adresa koji je u njemu sa mark_alloc_reg
// Sa tim da se fizicke adrese memorije moraju translirati u adresu regiona, dakle adresu date lokcije u regionu
// u rasponu od [0 , sz_regiona) 
void mark_alloc_mem( struct Memory * mem , unsigned int start , unsigned int end ){
  unsigned int end_index = end / SZ_4MB; // nadjemo index regiona u kojem je adresa do koje se treba mapirati
  unsigned int index; // problem sa granicnim adresama tipa od 4MB do 8MB se rijesi tako sto se funkcija mark_alloc_reg pri drugom poziju pozove sa start=end=0
  while( true ){
    index = start / SZ_4MB; // index regiona koji mapiramo
    if( index != end_index ){ // ako nismo dosli do posljednjeg regiona, mapiramo od starta do kraja datog regiona
      mark_alloc_reg( &mem->memory_[index] , start % SZ_4MB , SZ_4MB ); // translacija iz fizickih u regionalne adrese
      start = ++index * SZ_4MB; // podesimo start na vrijednost novog regiona
    }
    else{
      mark_alloc_reg( &mem->memory_[index] , start % SZ_4MB , end % SZ_4MB ); // ako smo dosli do kraja mapiramo od starta do enda
      break; // sa tim da ako je end umnozak 4MB desit imat cemo 0 ako radimo modul od SZ_4MB, ali taj problem je rijesen tako sto ce se pri tom slucaju index biti veci za 1
      // od stvarnog mapiranja, a u posljednjem mapiranju cemo imati mapiranje kod kojeg je start i end = 0
    }
  }
  return;
}

// Funckija mark_alloc() ne bi trebala biti nigdje koristena osim pri inicijaliaciji kernela, jer potencijalno moze narusiti strukturu alokatora
// jer ne provjerava da li je dati region memorije koji se oznacava kao alociran vec alociran prethodno sam ili u sklopu veceg komada memorije

int main(){
// testovi za cijelu memoriju
  struct Memory mem;
  allocation_Memory(&mem,0x04000000);
  init_Memory(&mem,0xF0000000);
  print_memory(&mem);
  PageInfo pg_info;
  
  
  malloc( &mem , SZ_4MB , &pg_info );
  print_memory(&mem);

  malloc( &mem , SZ_4KB , &pg_info );
  print_memory(&mem);

  free( &pg_info );
  print_memory(&mem);

  mark_alloc_mem(&mem ,0x00080000 , 0x00401000 );
  print_memory(&mem);

// testovi za region
/*
  struct MemoryRegion region;
  allocation_MemoryRegion(&region,0x1000,0x10); // komad velicine 4KB u kojem se mogu alocirati komadici velicine do 16 bajti
  init_MemoryRegion(&region);
  PageInfo pg_info;
  
  print_region(&region);
  std::cout << std::endl;
  // stanje memorije prije ikakvih alokacija
  
  if( mem_alloc(&region,0x100,&pg_info) ) //alociramo komad od 256 bajti
    std::cout << "Uspjesna alokacija" << std::endl;

  print_region(&region);
  std::cout << std::endl;
  // stanje memorije poslije prve alokacije
  
  mem_alloc(&region,0x80,&pg_info);
  
  print_region(&region);
  std::cout << std::endl;
  // stanje memorije poslije druge alokacije
  std::cout << REGION_ADDR( region.sz_of_region_ , pg_info.index_in_region_ ) << std::endl;

  mem_free(&pg_info);

  std::cout << "Poslije oslobadjanja" << std::endl;
  print_region(&region);
  std::cout << std::endl;
  // stanje memorije poslije oslobadjanja
*/
}
