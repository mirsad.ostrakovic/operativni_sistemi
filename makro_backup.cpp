#include<iostream>


#define TRAPHANDLER( a , b ) std::cout << "error" << STR(a) << std::endl;
#define TRAPHANDLER_NOEC( a , b ) std::cout << "no_error" << STR(b) << std::endl;


  #define COUNTER_LOOP_0(x,ext)                       x( ext ## 0 )
  #define COUNTER_LOOP_1(x,ext) COUNTER_LOOP_0(x,ext) x( ext ## 1 )
  #define COUNTER_LOOP_2(x,ext) COUNTER_LOOP_1(x,ext) x( ext ## 2 )
  #define COUNTER_LOOP_3(x,ext) COUNTER_LOOP_2(x,ext) x( ext ## 3 )
  #define COUNTER_LOOP_4(x,ext) COUNTER_LOOP_3(x,ext) x( ext ## 4 )
  #define COUNTER_LOOP_5(x,ext) COUNTER_LOOP_4(x,ext) x( ext ## 5 )
  #define COUNTER_LOOP_6(x,ext) COUNTER_LOOP_5(x,ext) x( ext ## 6 )
  #define COUNTER_LOOP_7(x,ext) COUNTER_LOOP_6(x,ext) x( ext ## 7 )
  #define COUNTER_LOOP_8(x,ext) COUNTER_LOOP_7(x,ext) x( ext ## 8 )
  #define COUNTER_LOOP_9(x,ext) COUNTER_LOOP_8(x,ext) x( ext ## 9 )


  #define COUNTER_LOOP_00(a,x,ext)                            COUNTER_LOOP_##a( x, ext ## 0 )
  #define COUNTER_LOOP_10(a,x,ext)  COUNTER_LOOP_00(9,x,ext)  COUNTER_LOOP_##a( x, ext ## 1 )
  #define COUNTER_LOOP_20(a,x,ext)  COUNTER_LOOP_10(9,x,ext)  COUNTER_LOOP_##a( x, ext ## 2 )
  #define COUNTER_LOOP_30(a,x,ext)  COUNTER_LOOP_20(9,x,ext)  COUNTER_LOOP_##a( x, ext ## 3 )
  #define COUNTER_LOOP_40(a,x,ext)  COUNTER_LOOP_30(9,x,ext)  COUNTER_LOOP_##a( x, ext ## 4 )
  #define COUNTER_LOOP_50(a,x,ext)  COUNTER_LOOP_40(9,x,ext)  COUNTER_LOOP_##a( x, ext ## 5 )
  #define COUNTER_LOOP_60(a,x,ext)  COUNTER_LOOP_50(9,x,ext)  COUNTER_LOOP_##a( x, ext ## 6 )
  #define COUNTER_LOOP_70(a,x,ext)  COUNTER_LOOP_60(9,x,ext)  COUNTER_LOOP_##a( x, ext ## 7 )
  #define COUNTER_LOOP_80(a,x,ext)  COUNTER_LOOP_70(9,x,ext)  COUNTER_LOOP_##a( x, ext ## 8 )
  #define COUNTER_LOOP_90(a,x,ext)  COUNTER_LOOP_80(9,x,ext)  COUNTER_LOOP_##a( x, ext ## 9 )

  #define COUNTER_LOOP_000(a,b,x,ext)                              COUNTER_LOOP_##a##0( b, x, ext ## 0 )
  #define COUNTER_LOOP_100(a,b,x,ext) COUNTER_LOOP_000(9,9,x,ext)  COUNTER_LOOP_##a##0( b, x, ext ## 1 )
  #define COUNTER_LOOP_200(a,b,x,ext) COUNTER_LOOP_100(9,9,x,ext)  COUNTER_LOOP_##a##0( b, x, ext ## 2 )
  #define COUNTER_LOOP_300(a,b,x,ext) COUNTER_LOOP_200(9,9,x,ext)  COUNTER_LOOP_##a##0( b, x, ext ## 3 )
  #define COUNTER_LOOP_400(a,b,x,ext) COUNTER_LOOP_300(9,9,x,ext)  COUNTER_LOOP_##a##0( b, x, ext ## 4 )
  #define COUNTER_LOOP_500(a,b,x,ext) COUNTER_LOOP_400(9,9,x,ext)  COUNTER_LOOP_##a##0( b, x, ext ## 5 )
  #define COUNTER_LOOP_600(a,b,x,ext) COUNTER_LOOP_500(9,9,x,ext)  COUNTER_LOOP_##a##0( b, x, ext ## 6 )
  #define COUNTER_LOOP_700(a,b,x,ext) COUNTER_LOOP_600(9,9,x,ext)  COUNTER_LOOP_##a##0( b, x, ext ## 7 )
  #define COUNTER_LOOP_800(a,b,x,ext) COUNTER_LOOP_700(9,9,x,ext)  COUNTER_LOOP_##a##0( b, x, ext ## 8 )
  #define COUNTER_LOOP_900(a,b,x,ext) COUNTER_LOOP_800(9,9,x,ext)  COUNTER_LOOP_##a##0( b, x, ext ## 9 )

  #define COUNTER_LOOP_1000(a,b,c,x) COUNTER_LOOP_##a##0##0(b,c,x,)

  #define COUNTER_LOOP(a,b,c,x) COUNTER_LOOP_1000(a,b,c,x)
  
  // posto smo definisali makro petlju koja ce ici od 0 do broja koji mi zadamo sa a , b , c i pozivati za svaki cijeli broj u tom opsegu x(broj)
  // gdje je x neki novi makro, moramo definisati makro koji ce u zavnisnosi od broj generisati TRAPHANDLER ili TRAPHANDLER_NOEC


  #define E_TRAPHANDLER( x ) TRAPHANDLER ( vector##x ,  x )
  #define E_TRAPHANDLER_NOEC( x ) TRAPHANDLER_NOEC ( vector##x , x )
  
  // makro kojima damo broj, a oni generisu unutar poziv makroa TRAPHANDLER ili TRAPHANDLER_NOEC
  // medjutim mi trebamo napraivit da za brojev 8 10 11 12 13 14 17 se pozove E_TRAPHANDLER, a za ostaloe E_TRAPHANDLER_NOEC

  #define SECOND(a,b,...) b
  // makro uzima 2 ili vise parametara, i ispisuje drugi, uz pomoc njega cemo izvesti jedan trik

  #define IS_PROBE_TRAP(FUN,x) SECOND( FUN , E_TRAPHANDLER_NOEC(x) )
  // IS_PROBE_TRAP ce pozvati SECOND() makro sa navedenik parametrima, i efektivno ce se ispisati drugi argument
  // sto ce biti E_TRAPHANDLER_NOEC(x) u slucaju da se FUN ne transforimise u vise parametara npr.2
 
  #define GEN_TRAPHANDLER( x ) IS_PROBE_TRAP( CAT( TRAPHANDLER_ , x ) , x ) 
  // spram gore navedenog ovdje prvi parametar postavimo kao TRAPHANDLER_x gdje je x parametar makroa
  // FUN u IS_PROBE_TRAP ce biti jednako ovom, sada ako je TRAPHANDLER_8 npr. definisan kao makro konsanta
  // koja se sastoji od dvije vrijednosti npr. ~ , E_TRAPHANDLER(8) , rezultat ce biti E_TRAPHANDLER(8), a ne
  // E_TRAPHANDLER_NOEC(8) jer ce ~ efektivno izvristi siftanje E_TRAPHNADLER na 2 mjesto, odnosno
  // E_TRAPHANDLER_NOEC na 3 mjesto


  #define TRAPHANDLER_008 ~ , E_TRAPHANDLER(008)
  #define TRAPHANDLER_010 ~ , E_TRAPHANDLER(010)
  #define TRAPHANDLER_011 ~ , E_TRAPHANDLER(011)
  #define TRAPHANDLER_012 ~ , E_TRAPHANDLER(012)
  #define TRAPHANDLER_013 ~ , E_TRAPHANDLER(013)
  #define TRAPHANDLER_014 ~ , E_TRAPHANDLER(014)
  #define TRAPHANDLER_017 ~ , E_TRAPHANDLER(017)

 



#define STR(X) #X
#define ISPIS(x) std::cout << STR( x )  << std::endl;

#define inc( a , b  , c )



int main(){


  COUNTER_LOOP(1,5,5,GEN_TRAPHANDLER)


/*
  std::cout << E_TRAPHANDLER( 5 ) << std::endl;
  std::cout << E_TRAPHANDLER_NOEC( 10 ) << std::endl;
  std::cout << BOOL(0) << std::endl;
  std::cout << BOOL(21) << std::endl;

  std:: cout << GEN_TRAPHANDLER( 0 ) << std::endl; 
  std:: cout << GEN_TRAPHANDLER( 4 ) << std::endl;
  std:: cout << GEN_TRAPHANDLER( 9 ) << std::endl;
  std:: cout << GEN_TRAPHANDLER( 12 ) << std::endl;
*/
  
}
