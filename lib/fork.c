// implement fork from user space

#include <inc/string.h>
#include <inc/lib.h>

// PTE_COW marks copy-on-write page table entries.
// It is one of the bits explicitly allocated to user processes (PTE_AVAIL).
#define PTE_COW		0x800

//
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
	void *addr = (void *) utf->utf_fault_va;
	uint32_t err = utf->utf_err;
	int r;

	// Check that the faulting access was (1) a write, and (2) to a
	// copy-on-write page.  If not, panic.
	// Hint:
	//   Use the read-only page table mappings at uvpt
	//   (see <inc/memlayout.h>).
  // LAB 4: Your code here.

  //pde_t * env_pgdir = (pde_t*)UVPT;
  //pde_t pde_entry = env_pgdir[ PDX(addr) ];
  //if( pde_entry & ( PTE_P | PTE_U | PTE_W ) != ( PTE_P | PTE_U | PTE_W ) )
  //  panic("pgfault() : PDE is not mapped appropriately\n");
  //pte_t pte_entry = (pte_t*)( KADDR( pde_entry & 0xFFFFF000 ) )[ PTX(addr) ];
  // isto posao se efektivno odradi u jednoj liniji koda, bez gore navedene provjere
  
  pte_t pte_entry;

  if( !( utf->utf_err & 0x2 ) ){ // FEW_WC = 0x2
    cprintf("Error code : %08x\n" , utf->utf_err );
    panic("pgfault() : page fault is not happend due write on not writable page\n");
  }
   
  if( ( uvpt[ PGNUM(addr) ] & ( PTE_P | PTE_U | PTE_COW ) ) != ( PTE_P | PTE_U | PTE_COW ) )
    panic("pgfault() : PT does not have approriately permission\n");
 
 
	// Allocate a new page, map it at a temporary location (PFTEMP),
	// copy the data from the old page to the new page, then move the new
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

	// LAB 4: Your code here.
  
  if( sys_page_alloc( 0 , PFTEMP , PTE_P | PTE_U | PTE_W ) )
    panic("pgfault() : problem with allocation\n");

  memmove( (void *)PFTEMP , (const void*)( ROUNDDOWN( addr , PGSIZE ) ) , PGSIZE );
  
  if( sys_page_map( 0 , (void*)PFTEMP , 0 , ROUNDDOWN( addr , PGSIZE ) , PTE_P | PTE_U | PTE_W ) )
    panic("pgfault() : problem with mapping\n");

  sys_page_unmap( 0 , (void *)PFTEMP );

  //if( utf->utf_eflags & FL_IF )
  //  cprintf("\t\t\t\tEFLAGS su ukljuceni\n");
  //else
  //  cprintf("\t\t\t\tEFLASI nisu ukljuceni\n");
}

//
// Map our virtual page pn (address pn*PGSIZE) into the target envid
// at the same virtual address.  If the page is writable or copy-on-write,
// the new mapping must be created copy-on-write, and then our mapping must be
// marked copy-on-write as well.  (Exercise: Why do we need to mark ours
// copy-on-write again if it was already copy-on-write at the beginning of
// this function?)
//
// Returns: 0 on success, < 0 on error.
// It is also OK to panic on error.
//
static int
duppage(envid_t envid, unsigned pn)
{ 
	int r = 0;

	// LAB 4: Your code here.
 
  int perm = PTE_P | PTE_U;
  uintptr_t addr = pn * PGSIZE;
 
  if( ( uvpt[ pn ] & PTE_W || uvpt[ pn ] & PTE_COW ) )
    perm |= PTE_COW;

  if( sys_page_map( 0 , (void *) addr , envid , (void *) addr , perm ) )
    panic("duppage() : problem with mapping\n");
  
  if( perm & PTE_COW )
    if( sys_page_map( 0 , (void *)addr , 0 , (void *)addr  , perm ) )
      panic("duppage() : problem with mapping\n");

  return 0;
	
}

// POJASNJENJE ZA UVPT mapiranje:
// na UVPT mi smo mapirali trenutni pgdir
// neka je adresa kojoj pristupamo u rasponu [UVPT,UVPT+PTSIZE)
// adresa se sastoji iz : PDX(addr) | PTX(addr) | offset
// i recimo da je trenutna adtresa = UVPT | PGSIZE | 3
// PDX(UVPT) je index entriju u trenutno PD, na kojem je mapiran ponovo isti PD(kao da je on PT)
// proces uradi prvi nivo indirekcije dodje ponovo u isti PD
// PTX(addr) sa ce u sustini biti novi index entrija u trenutnom PDu
// recimo da je to fiksno broj 10
// procesor ce naci 10 PTE entry i uraditi indirekciju
// efektivno mi smo sada presli u PT tabelu od datog PD entrija
// ostane nam da dodsamo jos samo offset
// recimo da je offset 4
// posto se velicine entrija 4 bajta
// mi cemo doce na adresu 1 PT entrija od 10 PDa

//
// User-level fork with copy-on-write.
// Set up our page fault handler appropriately.
// Create a child.
// Copy our address space and page fault handler setup to the child.
// Then mark the child as runnable and return.
//
// Returns: child's envid to the parent, 0 to the child, < 0 on error.
// It is also OK to panic on error.
//
// Hint:
//   Use uvpd, uvpt, and duppage.
//   Remember to fix "thisenv" in the child process.
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
// UVPT
envid_t
fork(void)
{
	// LAB 4: Your code here.
  envid_t child_envid;

  set_pgfault_handler( pgfault );
  child_envid = sys_exofork(); // kopiran je virtuelni procesor
 
  if( child_envid == 0 ){ // child_envid == 0 , radi se o procesu childu samo se vratimo
    thisenv = &envs[ ENVX( sys_getenvid() ) ];
    //cprintf( ( thisenv->env_tf.tf_eflags & FL_IF ) ? "\t\t\t\tUKLJUCENI EFLAGSI\n" : "\t\t\t\tISKLJUCENI EGLAGSI\n");
    return 0;
  }

  for( uintptr_t addr = 0u ; addr < UTOP ; ){ // kopiraju se sve stranice do kraja user dijela VA prostora
    if( !( uvpd[ PDX(addr) ] & PTE_P ) ){
    // cprintf("PDE koji se preskace index %08x\n", PDX(addr) ); 
      addr += PTSIZE;
      continue;
    }
      for( uint32_t i = 0u ; i < NPTENTRIES ; ++i ){ // prolazimo kroz sve entrije pte
      if( uvpt[ PGNUM(addr) ] & PTE_P && addr != UXSTACKTOP - PGSIZE ) // provjeravamo da li je mapiranje pristuno i razlicitmo od UXSTACKTOP - PGSIZE
        duppage( child_envid , PGNUM(addr) ); // ovaj nacin nije optimalan, ali zbog citkosti koda je ovdje napisano ovako 
      addr += PGSIZE;
    }
      
  }

  sys_page_alloc( child_envid , (void *)( UXSTACKTOP - PGSIZE ) , PTE_P | PTE_U | PTE_W ); // mapirana nova stranica za UXSTACK

  sys_env_set_pgfault_upcall( child_envid , thisenv->env_pgfault_upcall ); // postavljanje istig pgfault entry pointa

  sys_env_set_status(child_envid ,  ENV_RUNNABLE );

	return child_envid;
}

// Challenge!
int
sfork(void)
{
	panic("sfork not implemented");
	return -E_INVAL;
}
