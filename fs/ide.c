/*
 * Minimal PIO-based (non-interrupt-driven) IDE driver code.
 * For information about what all this IDE/ATA magic means,
 * see the materials available on the class references page.
 */

#include "fs.h"
#include <inc/x86.h>

#define IDE_BSY		0x80
#define IDE_DRDY	0x40
#define IDE_DF		0x20
#define IDE_ERR		0x01

static int diskno = 1;

// PORTovi 0x170 - 0x177 su od 2gog ATA harddisk controllera
// 0x177 je port za komandni registar/statusni registart ( 8 bitni registri )
// bit 0 ili ERR bit - indicira da se dogodila greska, potrebno je poslati novu komandu koja ce pocistit gresku ili pocistit je sa softwerski restartom
// bit 5 ili DF bit - drive fault error, koji ne stuje ERR bit
// bit 6 ili RDY bit - resetovan je na 0 poslije greske ili kada se drive od harddisak ne vrti odgovarajucom brzinom , inace je setovan na 1
// bit 7 ili BSY bit - indicira da drive se priprema da salje podatke, mora se pricekati odgovarajuci trenutak
// 0x176 je drive/head port - sluzi da odabir drive i/ili glave, mozda podrzava dodanti address/flag bit ( 8 bitni reg ) 
// 0x176 prva 4 bita su 0 pri davanju komande, 5 bit ili 4 indexom odabire drive
static int
ide_wait_ready(bool check_error)
{
	int r;

	while (((r = inb(0x1F7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY) // moramo pricekati dok harddisk ne bude spreman za novu komandu
		/* do nothing */;

	if (check_error && (r & (IDE_DF|IDE_ERR)) != 0) // ako postoji neka greska, dojaviti da postoji
		return -1;
	return 0;
}

bool
ide_probe_disk1(void)
{
	int r, x;

	// wait for Device 0 to be ready
	ide_wait_ready(0);

	// switch to Device 1
	outb(0x1F6, 0xE0 | (1<<4));

	// check for Device 1 to be ready for a while
	for (x = 0;
	     x < 1000 && ((r = inb(0x1F7)) & (IDE_BSY|IDE_DF|IDE_ERR)) != 0; /* provjera 1000x da li je drive spreman za koristenje */
	     x++)
		/* do nothing */;

	// switch back to Device 0
	outb(0x1F6, 0xE0 | (0<<4));

	cprintf("Device 1 presence: %d\n", (x < 1000));
	return (x < 1000); // ako je u nekoj iteraciji prekinut znaci da je drive prisutan 
}

void
ide_set_disk(int d)
{
	if (d != 0 && d != 1)
		panic("bad disk number");
	diskno = d;
}


int
ide_read(uint32_t secno, void *dst, size_t nsecs)
{
	int r;

	assert(nsecs <= 256);

	ide_wait_ready(0);

	outb(0x1F2, nsecs);
	outb(0x1F3, secno & 0xFF);
	outb(0x1F4, (secno >> 8) & 0xFF);
	outb(0x1F5, (secno >> 16) & 0xFF);
	outb(0x1F6, 0xE0 | ((diskno&1)<<4) | ((secno>>24)&0x0F));
	outb(0x1F7, 0x20);	// CMD 0x20 means read sector

	for (; nsecs > 0; nsecs--, dst += SECTSIZE) {
		if ((r = ide_wait_ready(1)) < 0)
			return r;
		insl(0x1F0, dst, SECTSIZE/4);
	}

	return 0;
}

int
ide_write(uint32_t secno, const void *src, size_t nsecs)
{
	int r;

	assert(nsecs <= 256);

	ide_wait_ready(0);

	outb(0x1F2, nsecs);
	outb(0x1F3, secno & 0xFF);
	outb(0x1F4, (secno >> 8) & 0xFF);
	outb(0x1F5, (secno >> 16) & 0xFF);
	outb(0x1F6, 0xE0 | ((diskno&1)<<4) | ((secno>>24)&0x0F));
	outb(0x1F7, 0x30);	// CMD 0x30 means write sector

	for (; nsecs > 0; nsecs--, src += SECTSIZE) {
		if ((r = ide_wait_ready(1)) < 0)
			return r;
		outsl(0x1F0, src, SECTSIZE/4);
	}

	return 0;
}

